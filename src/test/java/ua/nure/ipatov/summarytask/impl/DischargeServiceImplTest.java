package ua.nure.ipatov.summarytask.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import ua.nure.ipatov.summarytask.constant.TreatmentEnum;
import ua.nure.ipatov.summarytask.entity.Discharge;
import ua.nure.ipatov.summarytask.entity.Treatment;
import ua.nure.ipatov.summarytask.service.impl.DischargeServiceImpl;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static java.lang.System.lineSeparator;
import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.whenNew;

@RunWith(PowerMockRunner.class)
@PrepareForTest(DischargeServiceImpl.class)
public class DischargeServiceImplTest {

    @InjectMocks
    private DischargeServiceImpl dischargeService;

    @Mock
    private BufferedWriter writer;

    @Mock
    private FileWriter fileWriter;

    @Mock
    private Discharge discharge;

    @Mock
    private Treatment treatment;

    @Test
    public void EmptyTreatmentTest() throws Exception {

        List<Treatment> list = new ArrayList<>();

        whenNew(FileWriter.class).withArguments(System.getProperty("user.dir") + "/"
                + discharge.getName() + "_" + discharge.getSurname() + ".txt").thenReturn(fileWriter);
        whenNew(BufferedWriter.class).withArguments(fileWriter).thenReturn(writer);

        Mockito.when(discharge.getBirthday()).thenReturn(new Date(System.currentTimeMillis()));
        Mockito.when(discharge.getStartOfTreatment()).thenReturn(new Timestamp(System.currentTimeMillis()));

        Mockito.when(writer.append("DISCHARGE LIST")).thenReturn(writer);
        Mockito.when(writer.append(lineSeparator())).thenReturn(writer);
        Mockito.when(writer.append("Name: ")).thenReturn(writer);
        Mockito.when(writer.append(discharge.getName())).thenReturn(writer);
        Mockito.when(writer.append(lineSeparator())).thenReturn(writer);
        Mockito.when(writer.append("Surname: ")).thenReturn(writer);
        Mockito.when(writer.append(discharge.getSurname())).thenReturn(writer);
        Mockito.when(writer.append(lineSeparator())).thenReturn(writer);
        Mockito.when(writer.append("Birthday: ")).thenReturn(writer);
        Mockito.when(writer.append(discharge.getBirthday().toString())).thenReturn(writer);
        Mockito.when(writer.append(lineSeparator())).thenReturn(writer);
        Mockito.when(writer.append("Diagnosis: ")).thenReturn(writer);
        Mockito.when(writer.append(discharge.getDiagnosis())).thenReturn(writer);
        Mockito.when(writer.append(lineSeparator())).thenReturn(writer);
        Mockito.when(writer.append("Start of treatment: ")).thenReturn(writer);
        Mockito.when(writer.append(discharge.getStartOfTreatment().toString())).thenReturn(writer);
        Mockito.when(writer.append(lineSeparator())).thenReturn(writer);
        Mockito.when(writer.append("Treatments: ")).thenReturn(writer);
        Mockito.when(writer.append(lineSeparator())).thenReturn(writer);
        Mockito.when(writer.append("End of treatment: ")).thenReturn(writer);
        Mockito.when(writer.append(new Timestamp(System.currentTimeMillis()).toString())).thenReturn(writer);

        dischargeService.saveFile(discharge, list);

        verify(writer, times(7)).append(lineSeparator());
        verify(writer).append("DISCHARGE LIST");
        verify(writer).append("Name: ");
        verify(writer).append("Surname: ");
        verify(writer).append("Birthday: ");
        verify(writer).append("Diagnosis: ");
        verify(writer).append("Start of treatment: ");
        verify(writer).append("Treatments: ");
        verify(writer).append("End of treatment: ");
        verify(writer, times(3)).append(discharge.getName());
        verify(writer, times(3)).append(discharge.getSurname());
        verify(writer).append(discharge.getBirthday().toString());
        verify(writer).append(discharge.getStartOfTreatment().toString());
    }

    @Test
    public void WithTreatmentTest() throws Exception {

        List<Treatment> list = new ArrayList<>();
        list.add(treatment);

        whenNew(FileWriter.class).withArguments(System.getProperty("user.dir") + "/"
                + discharge.getName() + "_" + discharge.getSurname() + ".txt").thenReturn(fileWriter);
        whenNew(BufferedWriter.class).withArguments(fileWriter).thenReturn(writer);

        Mockito.when(discharge.getBirthday()).thenReturn(new Date(System.currentTimeMillis()));
        Mockito.when(discharge.getStartOfTreatment()).thenReturn(new Timestamp(System.currentTimeMillis()));
        Mockito.when(treatment.getTypeOfTreatmentsId()).thenReturn(TreatmentEnum.PROCEDURE);

        Mockito.when(writer.append("DISCHARGE LIST")).thenReturn(writer);
        Mockito.when(writer.append(lineSeparator())).thenReturn(writer);
        Mockito.when(writer.append("Name: ")).thenReturn(writer);
        Mockito.when(writer.append(discharge.getName())).thenReturn(writer);
        Mockito.when(writer.append(lineSeparator())).thenReturn(writer);
        Mockito.when(writer.append("Surname: ")).thenReturn(writer);
        Mockito.when(writer.append(discharge.getSurname())).thenReturn(writer);
        Mockito.when(writer.append(lineSeparator())).thenReturn(writer);
        Mockito.when(writer.append("Birthday: ")).thenReturn(writer);
        Mockito.when(writer.append(discharge.getBirthday().toString())).thenReturn(writer);
        Mockito.when(writer.append(lineSeparator())).thenReturn(writer);
        Mockito.when(writer.append("Diagnosis: ")).thenReturn(writer);
        Mockito.when(writer.append(discharge.getDiagnosis())).thenReturn(writer);
        Mockito.when(writer.append(lineSeparator())).thenReturn(writer);
        Mockito.when(writer.append("Start of treatment: ")).thenReturn(writer);
        Mockito.when(writer.append(discharge.getStartOfTreatment().toString())).thenReturn(writer);
        Mockito.when(writer.append(lineSeparator())).thenReturn(writer);
        Mockito.when(writer.append("Treatments: ")).thenReturn(writer);
        Mockito.when(writer.append(lineSeparator())).thenReturn(writer);
        Mockito.when(writer.append("-")).thenReturn(writer);
        Mockito.when(writer.append(treatment.getTypeOfTreatmentsId().toString())).thenReturn(writer);
        Mockito.when(writer.append(": ")).thenReturn(writer);
        Mockito.when(writer.append(treatment.getDescription())).thenReturn(writer);
        Mockito.when(writer.append(lineSeparator())).thenReturn(writer);
        Mockito.when(writer.append("End of treatment: ")).thenReturn(writer);
        Mockito.when(writer.append(new Timestamp(System.currentTimeMillis()).toString())).thenReturn(writer);

        dischargeService.saveFile(discharge, list);

        verify(writer, times(8)).append(lineSeparator());
        verify(writer).append("DISCHARGE LIST");
        verify(writer).append("Name: ");
        verify(writer).append("Surname: ");
        verify(writer).append("Birthday: ");
        verify(writer).append("Diagnosis: ");
        verify(writer).append("Start of treatment: ");
        verify(writer).append("Treatments: ");
        verify(writer).append(": ");
        verify(writer).append("-");
        verify(writer).append("End of treatment: ");
        verify(writer, times(4)).append(discharge.getName());
        verify(writer, times(4)).append(discharge.getSurname());
        verify(writer).append(discharge.getBirthday().toString());
        verify(writer).append(discharge.getStartOfTreatment().toString());
        verify(writer).append(treatment.getTypeOfTreatmentsId().toString());
        verify(writer, times(4)).append(treatment.getDescription());
    }

    @Test
    public void exceptionTest() throws Exception {

        List<Treatment> list = new ArrayList<>();

        whenNew(FileWriter.class).withArguments(System.getProperty("user.dir") + "/"
                + discharge.getName() + "_" + discharge.getSurname() + ".txt").thenThrow(new IOException());

        dischargeService.saveFile(discharge, list);

        Mockito.verify(writer, Mockito.never()).append(anyString());
    }
}
