package ua.nure.ipatov.summarytask.command.doctor;

import org.apache.log4j.Logger;
import ua.nure.ipatov.summarytask.command.Command;
import ua.nure.ipatov.summarytask.constant.TreatmentEnum;
import ua.nure.ipatov.summarytask.entity.*;
import ua.nure.ipatov.summarytask.service.CardService;
import ua.nure.ipatov.summarytask.service.DoctorService;
import ua.nure.ipatov.summarytask.service.PatientService;
import ua.nure.ipatov.summarytask.service.TreatmentService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

import static ua.nure.ipatov.summarytask.constant.Constant.*;
import static ua.nure.ipatov.summarytask.util.ConfigurationUtil.getConfigProperty;

/**
 * Create card command.
 *
 * @author Artem Ipatov
 */
public class CreateCardCommand extends Command {

    private CardService cardService;
    private TreatmentService treatmentService;
    private DoctorService doctorService;
    private PatientService patientService;
    private static final Logger log = Logger.getLogger(CreateCardCommand.class);

    public CreateCardCommand(CardService cardService, TreatmentService treatmentService,
                             DoctorService doctorService, PatientService patientService) {
        this.cardService = cardService;
        this.treatmentService = treatmentService;
        this.doctorService = doctorService;
        this.patientService = patientService;
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        log.debug("Command starts");

        String diagnosis = request.getParameter(DIAGNOSIS);
        log.trace("Request parameter: diagnosis --> " + diagnosis);

        String treatment = request.getParameter(TREATMENT);
        log.trace("Request parameter: treatment --> " + treatment);

        String description = request.getParameter(DESCRIPTION);
        log.trace("Request parameter: description --> " + description);

        int patientId = (int) request.getSession().getAttribute("selected");
        Card card = new Card();
        Treatment treatmentEntity = new Treatment();
        card.setDiagnosis(diagnosis);
        card.setPatientId(patientId);

        treatmentEntity.setDescription(description);
        treatmentEntity.setTypeOfTreatmentsId(TreatmentEnum.valueOf(treatment.toUpperCase()));
        treatmentEntity.setHospitalCardId(cardService.create(card));
        log.debug("Created card");

        treatmentService.create(treatmentEntity);
        log.debug("Created treatment");

        HttpSession session = request.getSession();
        User user = (User) request.getSession().getAttribute("user");
        Doctor doctor = doctorService.getDoctorByUserId(user.getId());
        log.trace("Found doctor by user id --> " + doctor);

        List<Patient> list = patientService.getPatientsWithoutCardByDoctorId(doctor.getId());
        log.trace("Found patients without card by doctor id --> " + list);

        session.setAttribute("patients", list);
        log.trace("Set session attribute: patients --> " + list);

        session.removeAttribute("selected");

        log.debug("Command finished");
        return getConfigProperty("path.redirect.doctor.start");
    }
}
