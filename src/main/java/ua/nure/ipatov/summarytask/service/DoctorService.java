package ua.nure.ipatov.summarytask.service;

import ua.nure.ipatov.summarytask.entity.Doctor;
import ua.nure.ipatov.summarytask.entity.dto.Checkin;

import java.util.List;

/**
 * Service for Doctor entity
 */
public interface DoctorService {

    /**
     * Create doctor.
     *
     * @param checkin data transfer object of doctor.
     */
    void create(Checkin checkin);

    /**
     * Get doctor by user id.
     *
     * @param id user id.
     * @return doctor entity.
     */
    Doctor getDoctorByUserId(int id);

    /**
     * Get all doctors.
     *
     * @return list of doctor entities.
     */
    List<Doctor> getAllDoctors();

    /**
     * Sort list of doctors.
     *
     * @param list list of doctor entities.
     * @param sort type of sort.
     * @return sorted list of doctor entities.
     */
    List<Doctor> sort(List<Doctor> list, String sort);
}
