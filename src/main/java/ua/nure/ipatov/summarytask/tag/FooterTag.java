package ua.nure.ipatov.summarytask.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.jsp.tagext.BodyTagSupport;
import java.io.IOException;

public class FooterTag extends BodyTagSupport {

    @Override
    public int doAfterBody() throws JspException {
        BodyContent bodyContent = this.bodyContent;
        String text = bodyContent.getString();
        JspWriter writer = bodyContent.getEnclosingWriter();

        try {
            writer.write(text);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return SKIP_BODY;
    }
}
