package ua.nure.ipatov.summarytask.util;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.security.NoSuchAlgorithmException;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

@RunWith(PowerMockRunner.class)
@PrepareForTest(PasswordEncoderUtil.class)
public class PasswordEncoderUtilTest {

    @Test
    public void matchesTest() throws Exception {
        String password = PasswordEncoderUtil.encodePassword("qwe");

        boolean result = PasswordEncoderUtil.matches("qwe", password);

        assertTrue(result);
    }

    @Test
    public void notMatchesTest() {
        String password = PasswordEncoderUtil.encodePassword("qwe");

        boolean result = PasswordEncoderUtil.matches("qwee", password);

        assertFalse(result);
    }

    @Test
    public void exceptionMatchesTest() throws Exception {
        mockStatic(PasswordEncoderUtil.class, Mockito.CALLS_REAL_METHODS);

        PowerMockito.doThrow(new NoSuchAlgorithmException())
                .when(PasswordEncoderUtil.class, "hash", anyString());

        assertFalse(PasswordEncoderUtil.matches("qwe", "qwe"));
    }

    @Test
    public void encodePasswordTest() throws Exception {
        String result = PasswordEncoderUtil.encodePassword("qwe");

        assertEquals(result, PasswordEncoderUtil.encodePassword("qwe"));
    }

    @Test
    public void exceptionEncodeTest() throws Exception {
        mockStatic(PasswordEncoderUtil.class, Mockito.CALLS_REAL_METHODS);

        PowerMockito.doThrow(new NoSuchAlgorithmException())
                .when(PasswordEncoderUtil.class, "hash", anyString());

        assertNull(PasswordEncoderUtil.encodePassword("qwe"));
    }
}
