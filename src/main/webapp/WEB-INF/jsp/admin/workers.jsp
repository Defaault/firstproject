<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>Workers</title>
    <link rel="stylesheet" href="style/workers.css">
</head>
<body>
   <%@ include file="/WEB-INF/jspf/header.jspf" %>
        <div class="root">
              <div class="firstTable">
                    <div>
                        <div class="h1">
                            <label><fmt:message key="workers.listOfDoctors"/></label>
                        </div>
                            <div class="tableContainer">
                                <table class="table">
                                    <tr>
                                        <th><fmt:message key="workers.id"/></th>
                                        <th><fmt:message key="workers.name"/></th>
                                        <th><fmt:message key="workers.surname"/></th>
                                        <th><fmt:message key="workers.patientCount"/></th>
                                        <th><fmt:message key="workers.specialty"/></th>
                                    </tr>
                                    <c:forEach items="${doctors}" var="doctor">
                                        <tr>
                                            <td>${doctor.id}</td>
                                            <td>${doctor.name}</td>
                                            <td>${doctor.surname}</td>
                                            <td>${doctor.patientCount}</td>
                                            <td><fmt:message key="${doctor.specialtyEnum.name}"/></td>
                                        </tr>
                                    </c:forEach>
                                </table>
                            </div>
                            <div class="sortForm">
                                <form action="controller">
                                    <input type="hidden" name="command" value="sortDoctors"/>
                                    <label class="font"><fmt:message key="workers.sort"/></label>
                                    <select class="select" name="sort">
                                        <c:forEach items="${sortDoctorList}" var="sort">
                                            <c:choose>
                                                <c:when test="${fn:toLowerCase(sort) eq selectedDoctorSort}">
                                                    <option value="${fn:toLowerCase(sort)}" selected><fmt:message key="${sort.name}"/></option>
                                                </c:when>
                                                <c:otherwise>
                                                    <option value="${fn:toLowerCase(sort)}"><fmt:message key="${sort.name}"/></option>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:forEach>
                                    </select>
                                    <input type="submit" value='<fmt:message key="workers.submit"/>'/>
                                </form>
                            </div>
                    </div>
              </div>
              <div class="secondTable">
                   <div>
                        <div class="h1">
                            <label><fmt:message key="workers.listOfPatients"/></label>
                        </div>
                        <article class="container">
                            <div class="tableContainer">
                                <table class="table">
                                    <tr>
                                        <th><fmt:message key="workers.id"/></td>
                                        <th><fmt:message key="workers.name"/></td>
                                        <th><fmt:message key="workers.surname"/></td>
                                        <th><fmt:message key="workers.birthday"/></td>
                                        <th><fmt:message key="workers.doctorId"/></td>
                                    <c:forEach items="${patients}" var="patient">
                                        <tr>
                                            <td>${patient.id}</td>
                                            <td>${patient.name}</td>
                                            <td>${patient.surname}</td>
                                            <td>${patient.birthday}</td>
                                            <td>${patient.doctorId}</td>
                                        </tr>
                                    </c:forEach>
                                </table>
                            </div>
                                <div class="sortForm">
                                    <form action="controller">
                                        <input type="hidden" name="command" value="sortPatients"/>
                                        <label class="font"><fmt:message key="workers.sort"/></label>
                                        <select class="select" name="sort">
                                            <c:forEach items="${sortPatientList}" var="sort">
                                                <c:choose>
                                                    <c:when test="${fn:toLowerCase(sort) eq selectedPatientSort}">
                                                        <option value="${fn:toLowerCase(sort)}" selected><fmt:message key="${sort.name}"/></option>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <option value="${fn:toLowerCase(sort)}"><fmt:message key="${sort.name}"/></option>
                                                    </c:otherwise>
                                                </c:choose>
                                            </c:forEach>
                                        </select>
                                        <input type="submit" value='<fmt:message key="workers.submit"/>'/>
                                    </form>
                                </div>
                        </article>
                   </div>
              </div>
            <%@ include file="/WEB-INF/jspf/footer.jspf" %>
      </div>
</body>
</html>