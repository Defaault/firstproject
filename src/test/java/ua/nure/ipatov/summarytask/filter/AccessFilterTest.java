package ua.nure.ipatov.summarytask.filter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import ua.nure.ipatov.summarytask.util.ConfigurationUtil;

import javax.servlet.FilterChain;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static ua.nure.ipatov.summarytask.util.ConfigurationUtil.getConfigProperty;

@RunWith(PowerMockRunner.class)
@PrepareForTest({AccessFilter.class, ConfigurationUtil.class})
public class AccessFilterTest {

    @Mock
    private HttpServletRequest servletRequest;

    @Mock
    private HttpServletResponse servletResponse;

    @Mock
    private FilterChain filterChain;

    @Mock
    private RequestDispatcher requestDispatcher;

    @InjectMocks
    private AccessFilter accessFilterSpy;

    @Before
    public void setUp() {
        accessFilterSpy = PowerMockito.spy(new AccessFilter());
    }

    @Test
    public void doFilterWithAccess() throws Exception {

        PowerMockito.doReturn(true).when(accessFilterSpy, "accessToPage", any());

        accessFilterSpy.doFilter(servletRequest, servletResponse, filterChain);

        verify(filterChain).doFilter(servletRequest, servletResponse);
    }

    @Test
    public void doFilterWithDeniedAccess() throws Exception {
        PowerMockito.mockStatic(ConfigurationUtil.class);

        PowerMockito.doReturn(false).when(accessFilterSpy, "accessToPage", any());
        PowerMockito.when(getConfigProperty("path.page.errorRights")).thenReturn("/WEB-INF/jsp/errorRights.jsp");
        doReturn(requestDispatcher).when(servletRequest).getRequestDispatcher("/WEB-INF/jsp/errorRights.jsp");

        accessFilterSpy.doFilter(servletRequest, servletResponse, filterChain);

        verify(requestDispatcher).forward(servletRequest, servletResponse);
    }
}
