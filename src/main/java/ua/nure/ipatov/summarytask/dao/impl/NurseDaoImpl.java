package ua.nure.ipatov.summarytask.dao.impl;

import org.apache.log4j.Logger;
import ua.nure.ipatov.summarytask.dao.NurseDao;
import ua.nure.ipatov.summarytask.entity.Nurse;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import static ua.nure.ipatov.summarytask.constant.Query.NURSER_SAVE;

public class NurseDaoImpl implements NurseDao {

    private static final Logger log = Logger.getLogger(NurseDaoImpl.class);

    @Override
    public void save(Nurse nurse, Connection connection) {

        try (PreparedStatement preparedStatement = connection.prepareStatement(NURSER_SAVE)) {

            preparedStatement.setString(1, nurse.getName());
            preparedStatement.setString(2, nurse.getSurname());
            preparedStatement.setInt(3, nurse.getUserId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            log.error(e.getMessage());

            try {
                connection.rollback();
            } catch (SQLException ex) {
                log.error(ex.getMessage());
            }
        }


    }
}
