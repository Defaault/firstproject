package ua.nure.ipatov.summarytask.filter;

import org.apache.log4j.Logger;

import javax.servlet.*;
import java.io.IOException;

/**
 * Encoding filter.
 *
 * @author Artem Ipatov
 */
public class EncodingFilter implements Filter {

    private String code;
    private static final Logger log = Logger.getLogger(EncodingFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        log.debug("Filter initialization starts");

        code = filterConfig.getInitParameter("encoding");

        log.trace("Encoding from web.xml --> " + code);
        log.debug("Filter initialization finished");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        log.debug("Filter starts");

        String encoding = servletRequest.getCharacterEncoding();

        if (code != null && !code.equalsIgnoreCase(encoding)) {
            log.trace("Request encoding != code, set encoding --> " + code);
            servletRequest.setCharacterEncoding(code);
        }

        log.debug("Filter finished");
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
