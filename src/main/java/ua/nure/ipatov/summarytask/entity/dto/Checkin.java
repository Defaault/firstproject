package ua.nure.ipatov.summarytask.entity.dto;

import ua.nure.ipatov.summarytask.constant.RoleEnum;
import ua.nure.ipatov.summarytask.constant.SpecialtyEnum;

/**
 * Checkin Data Transfer Object.
 *
 * @author Artem Ipatov
 */
public class Checkin {

    private String login;
    private String name;
    private String surname;
    private String password;
    private String repeatPassword;
    private RoleEnum roleEnum;
    private SpecialtyEnum specialtyEnum;

    public Checkin() {
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRepeatPassword() {
        return repeatPassword;
    }

    public void setRepeatPassword(String repeatPassword) {
        this.repeatPassword = repeatPassword;
    }

    public RoleEnum getRoleEnum() {
        return roleEnum;
    }

    public void setRoleEnum(RoleEnum roleEnum) {
        this.roleEnum = roleEnum;
    }

    public SpecialtyEnum getSpecialtyEnum() {
        return specialtyEnum;
    }

    public void setSpecialtyEnum(SpecialtyEnum specialtyEnum) {
        this.specialtyEnum = specialtyEnum;
    }
}
