function enabled() {

   var input = document.getElementById("select");
   var discharge = document.getElementById("selectToDischarge");

   if (input.length == 0) {
        document.getElementById("select").disabled = true;
        document.getElementById("selectToDischarge").disabled = true;
        document.getElementById("submit").disabled = true;
        document.getElementById("discharge").disabled = true;
   }
}