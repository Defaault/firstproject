package ua.nure.ipatov.summarytask.dao;

import ua.nure.ipatov.summarytask.entity.Card;

/**
 * Data access object for Card entity
 */
public interface CardDao {

    /**
     * Save card entity.
     *
     * @param card card to save.
     * @return card id.
     */
    int save(Card card);

    /**
     * Find card id by patient id.
     *
     * @param id patient id.
     * @return card id.
     */
    int findCardIdByPatientId(int id);

    /**
     * Delete all cards by patient id.
     *
     * @param id patient id.
     */
    void deleteAllByPatientId(int id);

    /**
     * Find all cards by patient id.
     *
     * @param id patient id.
     * @return card entity.
     */
    Card findByPatientId(int id);
}
