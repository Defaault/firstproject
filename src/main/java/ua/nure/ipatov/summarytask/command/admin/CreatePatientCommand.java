package ua.nure.ipatov.summarytask.command.admin;

import org.apache.log4j.Logger;
import ua.nure.ipatov.summarytask.command.Command;
import ua.nure.ipatov.summarytask.entity.Patient;
import ua.nure.ipatov.summarytask.service.PatientService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Date;
import java.util.Map;

import static ua.nure.ipatov.summarytask.constant.Constant.*;
import static ua.nure.ipatov.summarytask.util.ConfigurationUtil.getConfigProperty;
import static ua.nure.ipatov.summarytask.util.DataValidationUtil.validatePatient;

/**
 * Create patient command.
 *
 * @author Artem Ipatov
 */
public class CreatePatientCommand extends Command {

    private PatientService patientService;
    private static final Logger log = Logger.getLogger(CreatePatientCommand.class);

    public CreatePatientCommand(PatientService patientService) {
        this.patientService = patientService;
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        log.debug("Command starts");

        Patient patient = new Patient();
        patient.setName(request.getParameter(FIRST_NAME));
        patient.setSurname(request.getParameter(SECOND_NAME));
        patient.setBirthday(Date.valueOf(request.getParameter(BIRTHDAY)));
        HttpSession session = request.getSession();
        Map<String, String> message = validatePatient(patient);
        String random = request.getParameter("random");
        log.trace("Request parameter: random --> " + random);

        String selectRandom = request.getParameter("selectRandom");
        log.trace("Request parameter: selectRandom --> " + selectRandom);

        if (message.size() == 0) {
            log.debug("Data is valid");

            if ("yes".equals(random)) {
                log.debug("Selected random create patient");

                int result = patientService.createRandomly(patient, selectRandom);
                if (result == 0) {
                    log.debug("Created patient");

                    session.setAttribute("statusPatient", "status.signup.created");
                    log.trace("Set request attribute: statusPatient");

                    log.debug("Command finished");
                    return getConfigProperty("path.redirect.admin.start");
                } else {
                    log.debug("Not created patient");

                    message.put("NotExistsSpecialty", "errors.signup.createRandomlyPatient");
                    session.setAttribute("signup_patient_errors", message);

                    log.debug("Command finished");
                    return getConfigProperty("path.redirect.admin.start");
                }
            }

            patient.setDoctorId(Integer.parseInt(request.getParameter(DOCTOR_ID)));
            patientService.create(patient);
            log.debug("Created patient");

            session.setAttribute("statusPatient", "status.signup.created");
            log.trace("Set request attribute: statusPatient");

            session.setAttribute("signup_patient_errors", message);
            log.trace("Set request attribute: singup_patient_errors --> " + message);

            log.debug("Command finished");
            return getConfigProperty("path.redirect.admin.start");
        } else {
            log.debug("Data is invalid");

            session.setAttribute("signup_patient_errors", message);
            log.trace("Set request attribute: singup_patient_errors --> " + message);

            log.debug("Command finished");
            return getConfigProperty("path.redirect.admin.start");
        }
    }
}
