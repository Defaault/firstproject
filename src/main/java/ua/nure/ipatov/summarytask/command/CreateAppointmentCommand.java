package ua.nure.ipatov.summarytask.command;

import org.apache.log4j.Logger;
import ua.nure.ipatov.summarytask.constant.RoleEnum;
import ua.nure.ipatov.summarytask.constant.TreatmentEnum;
import ua.nure.ipatov.summarytask.entity.Doctor;
import ua.nure.ipatov.summarytask.entity.Patient;
import ua.nure.ipatov.summarytask.entity.Treatment;
import ua.nure.ipatov.summarytask.entity.User;
import ua.nure.ipatov.summarytask.service.CardService;
import ua.nure.ipatov.summarytask.service.DoctorService;
import ua.nure.ipatov.summarytask.service.PatientService;
import ua.nure.ipatov.summarytask.service.TreatmentService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

import static ua.nure.ipatov.summarytask.constant.Constant.*;
import static ua.nure.ipatov.summarytask.util.ConfigurationUtil.getConfigProperty;

/**
 * Create appointment command.
 *
 * @author Artem Ipatov
 */
public class CreateAppointmentCommand extends Command {

    private CardService cardService;
    private DoctorService doctorService;
    private PatientService patientService;
    private TreatmentService treatmentService;

    private static final Logger log = Logger.getLogger(CreateAppointmentCommand.class);

    public CreateAppointmentCommand(CardService cardService, DoctorService doctorService,
                                    PatientService patientService, TreatmentService treatmentService) {
        this.cardService = cardService;
        this.doctorService = doctorService;
        this.patientService = patientService;
        this.treatmentService = treatmentService;
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        log.debug("Command starts");

        Treatment treatmentEntity = new Treatment();
        List<Patient> list;
        String page;

        String treatment = request.getParameter(TREATMENT);
        log.trace("Request parameter: treatment --> " + treatment);

        String description = request.getParameter(DESCRIPTION);
        log.trace("Request parameter: description --> " + description);

        int patient = (int) request.getSession().getAttribute("selected");
        HttpSession session = request.getSession();
        int id = cardService.getCardIdByPatientId(patient);
        log.trace("Found card id by patient id --> " + id);

        treatmentEntity.setDescription(description);
        treatmentEntity.setTypeOfTreatmentsId(TreatmentEnum.valueOf(treatment.toUpperCase()));
        treatmentEntity.setHospitalCardId(id);
        treatmentService.create(treatmentEntity);
        log.debug("Created treatment");

        User user = (User) session.getAttribute(USER);
        Doctor doctor = doctorService.getDoctorByUserId(user.getId());
        log.trace("Found doctor by user ID --> " + doctor);

        if (user.getRoleEnum() == RoleEnum.DOCTOR) {
            log.trace("User has role --> " + user.getRoleEnum());

            list = patientService.getPatientsWithDiagnosisByDoctorId(doctor.getId());
            log.trace("Found patients with diagnosis by doctor id --> " + list);

            page = getConfigProperty("path.redirect.doctor.appointment");
        } else {
            log.trace("User has role --> " + user.getRoleEnum());

            list = patientService.getPatientsWithDiagnosis();
            log.trace("Found patients with diagnosis");

            page = getConfigProperty("path.redirect.nurse.start");
        }
        session.setAttribute("patientsWithDiagnosis", list);
        log.trace("Set session attribute: patientsWithDiagnosis --> " + list);

        session.removeAttribute("selected");

        log.debug("Command finished");
        return page;
    }
}
