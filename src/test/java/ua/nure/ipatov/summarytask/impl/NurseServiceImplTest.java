package ua.nure.ipatov.summarytask.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import ua.nure.ipatov.summarytask.dao.NurseDao;
import ua.nure.ipatov.summarytask.dao.UserDao;
import ua.nure.ipatov.summarytask.entity.dto.Checkin;
import ua.nure.ipatov.summarytask.service.impl.NurseServiceImpl;
import ua.nure.ipatov.summarytask.util.DBManagerUtil;
import ua.nure.ipatov.summarytask.util.PasswordEncoderUtil;

import java.sql.Connection;
import java.sql.SQLException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

@RunWith(PowerMockRunner.class)
@PrepareForTest({PasswordEncoderUtil.class, DBManagerUtil.class})
public class NurseServiceImplTest {

    @InjectMocks
    private NurseServiceImpl nurseService;

    @Mock
    private Checkin checkin;

    @Mock
    private UserDao userDao;

    @Mock
    private NurseDao nurseDao;

    @Mock
    private Connection connection;

    @Mock
    private DBManagerUtil dbManagerUtil;

    @Test
    public void createTest() {
        mockStatic(PasswordEncoderUtil.class);
        mockStatic(DBManagerUtil.class);

        PowerMockito.when(DBManagerUtil.getInstance()).thenReturn(dbManagerUtil);
        PowerMockito.when(dbManagerUtil.getConnection()).thenReturn(connection);

        nurseService.create(checkin);

        verify(userDao).save(any(), any());
        verify(nurseDao).save(any(), any());
    }

    @Test
    public void errorCreateTest() throws SQLException {
        mockStatic(PasswordEncoderUtil.class);
        mockStatic(DBManagerUtil.class);

        PowerMockito.when(DBManagerUtil.getInstance()).thenReturn(dbManagerUtil);
        PowerMockito.when(dbManagerUtil.getConnection()).thenReturn(connection);
        doThrow(new SQLException()).when(connection).setAutoCommit(false);

        nurseService.create(checkin);

        verify(userDao, never()).save(any(), any());
        verify(nurseDao, never()).save(any(), any());
    }
}
