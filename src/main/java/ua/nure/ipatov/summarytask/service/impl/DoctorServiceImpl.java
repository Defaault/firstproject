package ua.nure.ipatov.summarytask.service.impl;

import org.apache.log4j.Logger;
import ua.nure.ipatov.summarytask.dao.DoctorDao;
import ua.nure.ipatov.summarytask.dao.UserDao;
import ua.nure.ipatov.summarytask.entity.Doctor;
import ua.nure.ipatov.summarytask.entity.User;
import ua.nure.ipatov.summarytask.entity.dto.Checkin;
import ua.nure.ipatov.summarytask.service.DoctorService;
import ua.nure.ipatov.summarytask.util.DBManagerUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;
import java.util.ResourceBundle;

import static java.util.Objects.nonNull;
import static ua.nure.ipatov.summarytask.constant.Constant.*;
import static ua.nure.ipatov.summarytask.util.PasswordEncoderUtil.encodePassword;

public class DoctorServiceImpl implements DoctorService {

    private DoctorDao doctorDao;
    private UserDao userDao;
    private static final Logger log = Logger.getLogger(DoctorServiceImpl.class);

    public DoctorServiceImpl(DoctorDao doctorDao, UserDao userDao) {
        this.doctorDao = doctorDao;
        this.userDao = userDao;
    }

    @Override
    public void create(Checkin checkin) {
        Doctor doctor = new Doctor();
        User user = new User();

        user.setLogin(checkin.getLogin());
        user.setPassword(encodePassword(checkin.getPassword()));
        user.setRoleEnum(checkin.getRoleEnum());

        doctor.setName(checkin.getName());
        doctor.setSurname(checkin.getSurname());
        doctor.setPatientCount(0);
        doctor.setSpecialtyEnum(checkin.getSpecialtyEnum());

        try (Connection connection = DBManagerUtil.getInstance().getConnection()) {
            connection.setAutoCommit(false);
            int id = userDao.save(user, connection);
            doctor.setUserId(id);
            doctorDao.save(doctor, connection);
            connection.commit();
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public Doctor getDoctorByUserId(int id) {
        return doctorDao.findByUserId(id);
    }

    @Override
    public List<Doctor> getAllDoctors() {
        return doctorDao.findAllDoctors();
    }

    @Override
    public List<Doctor> sort(List<Doctor> list, String sort) {
        if (nonNull(list)) {
            if (ALPHABET.equals(sort)) {
                return alphabetSort(list);
            } else if (PATIENT.equals(sort)) {
                return patientSort(list);
            } else if (SPECIALTY.equals(sort)) {
                return specialtySort(list);
            }
        }
        return list;
    }

    private List<Doctor> alphabetSort(List<Doctor> list) {
        list.sort((doctor, t1) -> Integer.compare(doctor.getName().compareTo(t1.getName()), 0));
        return list;
    }

    private List<Doctor> patientSort(List<Doctor> list) {
        list.sort(Comparator.comparingInt(Doctor::getPatientCount));
        return list;
    }

    private List<Doctor> specialtySort(List<Doctor> list) {
        ResourceBundle resourceBundle = ResourceBundle.getBundle("resources");
        list.sort((doctor, t1) -> Integer.compare(resourceBundle.getString(doctor.getSpecialtyEnum().getName()).compareTo(resourceBundle.getString(t1.getSpecialtyEnum().getName())), 0));
        return list;
    }
}
