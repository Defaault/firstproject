package ua.nure.ipatov.summarytask.filter;

import org.apache.log4j.Logger;
import ua.nure.ipatov.summarytask.constant.RoleEnum;
import ua.nure.ipatov.summarytask.entity.User;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Objects.isNull;
import static ua.nure.ipatov.summarytask.util.ConfigurationUtil.getConfigProperty;

/**
 * Security filter.
 *
 * @author Artem Ipatov
 */
public class AccessFilter implements Filter {

    private Map<RoleEnum, List<String>> roleMap = new HashMap<>();
    private List<String> common;
    private List<String> outOfControl;
    private static final Logger log = Logger.getLogger(AccessFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        log.debug("Filter initialization starts");

        roleMap.put(RoleEnum.ADMIN, getParameters(filterConfig.getInitParameter("admin")));
        roleMap.put(RoleEnum.DOCTOR, getParameters(filterConfig.getInitParameter("doctor")));
        roleMap.put(RoleEnum.NURSE, getParameters(filterConfig.getInitParameter("nurse")));
        log.trace("Role map --> " + roleMap);

        common = getParameters(filterConfig.getInitParameter("common"));
        log.trace("Common commands --> " + common);

        outOfControl = getParameters(filterConfig.getInitParameter("out-of-control"));
        log.trace("Out of control commands --> " + outOfControl);

        log.debug("Filter initialization finished");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        log.debug("Filter start");

        if (accessToPage(servletRequest)) {
            log.debug("Filter finished");
            filterChain.doFilter(servletRequest, servletResponse);
        } else {
            log.debug("Access is denied");
            servletRequest.getRequestDispatcher(getConfigProperty("path.page.errorRights")).forward(servletRequest, servletResponse);
        }
    }

    private boolean accessToPage(ServletRequest servletRequest) {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        String command = request.getParameter("command");

        if (outOfControl.contains(command)) {
            return true;
        }

        User user = (User) request.getSession().getAttribute("user");
        if (isNull(user)) {
            return false;
        }

        if (common.contains(command)) {
            return true;
        }
        RoleEnum roleEnum = user.getRoleEnum();

        switch (roleEnum) {
            case ADMIN:
                return roleMap.get(RoleEnum.ADMIN).contains(command);
            case DOCTOR:
                return roleMap.get(RoleEnum.DOCTOR).contains(command);
            case NURSE:
                return roleMap.get(RoleEnum.NURSE).contains(command);
        }
        return false;
    }

    /**
     * Extracts parameter values from string
     *
     * @param params parameter value string
     * @return list of parameter values.
     */
    private List<String> getParameters(String params) {
        return Arrays.asList(params.split(" "));
    }
}
