package ua.nure.ipatov.summarytask.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import ua.nure.ipatov.summarytask.dao.impl.DoctorDaoImpl;
import ua.nure.ipatov.summarytask.dao.impl.PatientDaoImpl;
import ua.nure.ipatov.summarytask.entity.Patient;
import ua.nure.ipatov.summarytask.service.impl.PatientServiceImpl;
import ua.nure.ipatov.summarytask.util.DBManagerUtil;

import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

@RunWith(PowerMockRunner.class)
@PrepareForTest(DBManagerUtil.class)
public class PatientServiceImplTest {

    @InjectMocks
    private PatientServiceImpl patientService;

    @Mock
    private DoctorDaoImpl doctorDao;

    @Mock
    private PatientDaoImpl patientDao;

    @Mock
    private Patient patient;

    @Mock
    private Connection connection;

    @Mock
    private DBManagerUtil dbManagerUtil;

    @Test
    public void alphabetSortTest() {
        Patient patient = new Patient();
        Patient secondPatient = new Patient();
        List<Patient> list = new ArrayList<>();
        List<Patient> result = new ArrayList<>();
        patient.setName("Artem");
        secondPatient.setName("Dima");

        result.add(patient);
        result.add(secondPatient);
        list.add(secondPatient);
        list.add(patient);

        patientService.sort(list, "alphabet");

        assertEquals(result, list);
    }

    @Test
    public void birthdaySortTest() {
        Patient patient = new Patient();
        Patient secondPatient = new Patient();
        List<Patient> list = new ArrayList<>();
        List<Patient> result = new ArrayList<>();
        patient.setBirthday(new Date(System.currentTimeMillis()));
        secondPatient.setBirthday(new Date(System.currentTimeMillis() + System.currentTimeMillis()));

        result.add(patient);
        result.add(secondPatient);
        list.add(secondPatient);
        list.add(patient);

        patientService.sort(list, "birthday");

        assertEquals(result, list);
    }

    @Test
    public void nullListSortTest() {

        patientService.sort(null, "patient");

        assertNull(null);
    }

    @Test
    public void createRandomlyAllTest() {
        mockStatic(DBManagerUtil.class);

        when(doctorDao.findDoctorIdWithMinPatient()).thenReturn(1);
        PowerMockito.when(DBManagerUtil.getInstance()).thenReturn(dbManagerUtil);
        PowerMockito.when(dbManagerUtil.getConnection()).thenReturn(connection);

        int result = patientService.createRandomly(patient, "all");

        verify(patient).setDoctorId(1);
        verify(patientDao).save(any(), any());
        verify(doctorDao).incrementCountPatients(anyInt(), any());
        assertEquals(0, result);
    }

    @Test
    public void errorCreateRandomlyAllTest() throws SQLException {
        mockStatic(DBManagerUtil.class);

        when(doctorDao.findDoctorIdWithMinPatient()).thenReturn(1);
        PowerMockito.when(DBManagerUtil.getInstance()).thenReturn(dbManagerUtil);
        PowerMockito.when(dbManagerUtil.getConnection()).thenReturn(connection);
        doThrow(new SQLException()).when(connection).setAutoCommit(false);

        int result = patientService.createRandomly(patient, "all");

        verify(patientDao, never()).save(any(), any());
        verify(doctorDao, never()).incrementCountPatients(anyInt(), any());
        assertEquals(0, result);
    }

    @Test
    public void createRandomlyNotAllTest() {
        mockStatic(DBManagerUtil.class);

        when(doctorDao.findDoctorIdBySpecialtyWithMinPatient("pediatrician")).thenReturn(1);
        PowerMockito.when(DBManagerUtil.getInstance()).thenReturn(dbManagerUtil);
        PowerMockito.when(dbManagerUtil.getConnection()).thenReturn(connection);

        int result = patientService.createRandomly(patient, "pediatrician");

        verify(patient).setDoctorId(1);
        verify(patientDao).save(any(), any());
        verify(doctorDao).incrementCountPatients(anyInt(), any());
        assertEquals(0, result);
    }

    @Test
    public void errorCreateRandomlyNotAllTest() throws SQLException {
        mockStatic(DBManagerUtil.class);

        when(doctorDao.findDoctorIdBySpecialtyWithMinPatient("pediatrician")).thenReturn(1);
        PowerMockito.when(DBManagerUtil.getInstance()).thenReturn(dbManagerUtil);
        PowerMockito.when(dbManagerUtil.getConnection()).thenReturn(connection);
        doThrow(new SQLException()).when(connection).setAutoCommit(false);

        int result = patientService.createRandomly(patient, "pediatrician");

        verify(patientDao, never()).save(any(), any());
        verify(doctorDao, never()).incrementCountPatients(anyInt(), any());
        assertEquals(0, result);
    }

    @Test
    public void createRandomlyWithDoctorIdNullTest() {

        when(doctorDao.findDoctorIdWithMinPatient()).thenReturn(0);

        int result = patientService.createRandomly(patient, "all");

        assertEquals(1, result);
    }

}
