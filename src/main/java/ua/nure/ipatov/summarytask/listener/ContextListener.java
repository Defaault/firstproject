package ua.nure.ipatov.summarytask.listener;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import ua.nure.ipatov.summarytask.constant.LocaleEnum;
import ua.nure.ipatov.summarytask.constant.RoleEnum;
import ua.nure.ipatov.summarytask.constant.SpecialtyEnum;
import ua.nure.ipatov.summarytask.constant.TreatmentEnum;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.util.EnumSet;

/**
 * Context listener.
 *
 * @author Artem Ipatov
 */
public class ContextListener implements ServletContextListener {

    private static final Logger log = Logger.getLogger(ContextListener.class);

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ServletContext servletContext = sce.getServletContext();
        initLog4J(servletContext);
        initI18N(servletContext);
        initCommandContainer();
        initRoles(servletContext);
        initSpecializations(servletContext);
        initDoctorTreatments(servletContext);
        initNurseTreatments(servletContext);
    }

    /**
     * Initialize I18N
     */
    private void initI18N(ServletContext servletContext) {
        log.debug("initI18N initialization started");
        EnumSet<LocaleEnum> localeEnums = EnumSet.range(LocaleEnum.EN, LocaleEnum.RU);
        servletContext.setAttribute("locales", localeEnums);
        log.debug("initI18N initialization finished");
    }

    /**
     * Initialize all roles and set to session.
     */
    private void initRoles(ServletContext servletContext) {
        log.debug("initRoles initialization started");
        EnumSet<RoleEnum> roleEnums = EnumSet.range(RoleEnum.DOCTOR, RoleEnum.NURSE);
        servletContext.setAttribute("roles", roleEnums);
        log.debug("initRoles initialization finished");
    }

    /**
     * Initialize all specializations and set to session.
     */
    private void initSpecializations(ServletContext servletContext) {
        log.debug("initSpecializations initialization started");
        EnumSet<SpecialtyEnum> specialtyEnums = EnumSet.range(SpecialtyEnum.SURGEON, SpecialtyEnum.PEDIATRICIAN);
        servletContext.setAttribute("specializations", specialtyEnums);
        log.debug("initSpecializations initialization finished");
    }

    /**
     * Initialize all doctor treatments.
     */
    private void initDoctorTreatments(ServletContext servletContext) {
        log.debug("initDoctorTreatments initialization started");
        EnumSet<TreatmentEnum> treatmentEnums = EnumSet.range(TreatmentEnum.PROCEDURE, TreatmentEnum.OPERATION);
        servletContext.setAttribute("doctorTreatments", treatmentEnums);
        log.debug("initDoctorTreatments initialization finished");
    }

    /**
     * Initialize all nurse treatments.
     */
    private void initNurseTreatments(ServletContext servletContext) {
        log.debug("initNurseTreatments initialization started");
        EnumSet<TreatmentEnum> treatmentEnums = EnumSet.range(TreatmentEnum.PROCEDURE, TreatmentEnum.MEDICINE);
        servletContext.setAttribute("nurseTreatments", treatmentEnums);
        log.debug("initNurseTreatments initialization finished");
    }

    /**
     * Initialize command container
     */
    private void initCommandContainer() {
        log.debug("initCommandContainer initialization started");
        try {
            Class.forName("ua.nure.ipatov.summarytask.container.CommandContainer");
        } catch (ClassNotFoundException e) {
            log.error("Not found CommandContainer class");
        }
        log.debug("initCommandContainer initialization finished");
    }

    /**
     * Initialize Log4J framework.
     */
    private void initLog4J(ServletContext servletContext) {
        log("Log4J initialization started");
        PropertyConfigurator.configure(servletContext.getRealPath("WEB-INF/log4j.properties"));
        log("Log4J initialization finished");
    }

    private void log(String msg) {
        System.out.println("[ContextListener] " + msg);
    }
}
