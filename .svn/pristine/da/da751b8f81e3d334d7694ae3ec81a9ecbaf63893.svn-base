package ua.nure.ipatov.summarytask.constant;

/**
 * Query holder.
 *
 * @author Artem Ipatov
 */
public final class Query {

    public static final String HOSPITAL_SAVE = "insert into hospital_cards(diagnosis, id_patient) values(?,?)";
    public static final String FIND = "select id from hospital_cards where id_patient = ?";
    public static final String FIND_CARDS_BY_PATIENT_ID = "select * from hospital_cards where id_patient = ?";
    public static final String DELETE_ALL_BY_PATIENT_ID = "delete from hospital_cards where id_patient = ?";
    public static final String DISCHARGE_SAVE = "insert into discharged_patients" +
            "(name,surname, birthday, diagnosis, start_of_treatment) values(?,?,?,?,?)";
    public static final String FIND_ALL = "select *,(select specialty from specializations where id_specialty = id) " +
            "from doctors";
    public static final String DOCTORS_SAVE = "insert into doctors(name,surname,patient_count,id_specialty,id_user) " +
            "values(?,?,?,?,?)";
    public static final String INCREMENT_COUNT_PATIENTS = "update doctors set patient_count = patient_count + 1 where id = ?";
    public static final String FIND_BY_USER_ID = "select *,(select specialty from specializations where id = id_specialty) " +
            "from doctors where id_user = ?";
    public static final String NURSER_SAVE = "insert into nurses(name,surname,id_user) values(?,?,?)";
    public static final String PATIENTS_SAVE = "insert into patients(name,surname,birthday,id_doctor) values(?,?,?,?)";
    public static final String FIND_ALL_PATIENTS = "select * from patients";
    public static final String FIND_ALL_WITHOUT_CARD_BY_DOCTOR_ID = "select * from patients where " +
            "id != all (select id_patient from hospital_cards) and id_doctor = ?";
    public static final String FIND_ALL_WITH_DIAGNOSIS = "select * from patients where " +
            "id = any (select id_patient from hospital_cards)";
    public static final String FIND_ALL_WITH_DIAGNOSIS_BY_DOCTOR_ID = "select * from patients where " +
            "id = any (select id_patient from hospital_cards) and id_doctor = ?";
    public static final String FIND_PATIENT = "select * from patients where id = ?";
    public static final String DELETE_BY_ID = "delete from patients where id = ?";
    public static final String SAVE_TREATMENTS = "insert into treatments" +
            "(description, id_type_of_treatment, id_hospital_card) values(?,?,?)";
    public static final String DELETE_ALL_BY_CARD_ID = "delete from treatments where id_hospital_card = ?";
    public static final String TAKE_USER = "select *, (select role from roles where id_role = id) " +
            "from users where login = ?";
    public static final String SAVE_USER = "insert into users(login,password,id_role) values(?,?,?)";
    public static final String UPDATE_LOCALE = "update users set locale = ? where id = ?";
    public static final String FIND_DISCHARGE = "select * from discharged_patients where name = ? and surname = ?";
    public static final String FIND_TREATMENTS_BY_PATIENT_ID = "select *, (select `type` from type_of_treatments" +
            " where id_type_of_treatment = type_of_treatments.id) from treatments " +
            "where id_hospital_card = (select id from hospital_cards where id_patient = ?)";
    public static final String FIND_DOCTOR_ID_WITH_MIN_PATIENT = "select * from doctors where" +
            " patient_count = (select min(patient_count) from doctors) limit 1;";
    public static final String FIND_DOCTOR_ID_BY_SPECIALTY_WITH_MIN_PATIENT = "select * from doctors where" +
            " id_specialty = (select id from specializations where specialty = ?) order by patient_count asc limit 1";
    public static final String DECREMENT_COUNT_PATIENTS = "update doctors set patient_count = patient_count - 1 where id = ?";

    private Query() {
    }
}
