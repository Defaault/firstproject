package ua.nure.ipatov.summarytask.constant;

/**
 * Role constants.
 *
 * @author Artem Ipatov
 */
public enum RoleEnum {
    ADMIN(1, "role.admin"),
    DOCTOR(2, "role.doctor"),
    NURSE(3, "role.nurse");

    RoleEnum(int id, String name) {
        this.id = id;
        this.name = name;
    }

    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
