package ua.nure.ipatov.summarytask.util;

import org.apache.log4j.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

import static java.util.Objects.isNull;

/**
 * DB manager. Works with MySQL DB.
 *
 * @author Artem Ipatov
 */
public final class DBManagerUtil {

    private static DBManagerUtil dbManagerUtil = null;
    private static final Logger log = Logger.getLogger(DBManagerUtil.class);

    private DBManagerUtil() {
    }

    public static DBManagerUtil getInstance() {

        if (isNull(dbManagerUtil)) {
            dbManagerUtil = new DBManagerUtil();
        }
        return dbManagerUtil;
    }

    /**
     * Returns a DB connection from the Pool Connections. Before using this
     * method you must configure the Date Source and the Connections Pool in your
     * WEB_APP_ROOT/META-INF/context.xml file.
     *
     * @return A DB connection.
     */
    public Connection getConnection() {
        Connection connection = null;
        try {
            Context envCtx = (Context) (new InitialContext().lookup("java:comp/env"));
            DataSource ds = (DataSource) envCtx.lookup("jdbc/SummaryTask");
            connection = ds.getConnection();
        } catch (SQLException | NamingException e) {
            log.error(e.getMessage());
        }
        return connection;
    }
}
