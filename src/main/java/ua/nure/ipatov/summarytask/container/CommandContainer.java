package ua.nure.ipatov.summarytask.container;

import org.apache.log4j.Logger;
import ua.nure.ipatov.summarytask.command.*;
import ua.nure.ipatov.summarytask.command.admin.*;
import ua.nure.ipatov.summarytask.command.doctor.CreateCardCommand;
import ua.nure.ipatov.summarytask.command.doctor.DischargeCommand;
import ua.nure.ipatov.summarytask.command.doctor.ViewCardCommand;
import ua.nure.ipatov.summarytask.dao.*;
import ua.nure.ipatov.summarytask.dao.impl.*;
import ua.nure.ipatov.summarytask.service.*;
import ua.nure.ipatov.summarytask.service.impl.*;

import java.util.HashMap;
import java.util.Map;

import static java.util.Objects.isNull;

/**
 * Holder for all commands and partly realization dependency injection pattern
 *
 * @author Artem Ipatov
 */
public class CommandContainer {

    private static Map<String, Command> commands = new HashMap<>();
    private static final Logger log = Logger.getLogger(CommandContainer.class);

    static {
        UserDao userDao = new UserDaoImpl();
        CardDao cardDao = new CardDaoImpl();
        DischargeDao dischargeDao = new DischargeDaoImpl();
        DoctorDao doctorDao = new DoctorDaoImpl();
        NurseDao nurseDao = new NurseDaoImpl();
        PatientDao patientDao = new PatientDaoImpl();
        TreatmentDao treatmentDao = new TreatmentDaoImpl();
        UserService userService = new UserServiceImpl(userDao);
        CardService cardService = new CardServiceImpl(cardDao);
        DischargeService dischargeService = new DischargeServiceImpl(dischargeDao, doctorDao, patientDao);
        DoctorService doctorService = new DoctorServiceImpl(doctorDao, userDao);
        NurseService nurseService = new NurseServiceImpl(nurseDao, userDao);
        PatientService patientService = new PatientServiceImpl(patientDao, doctorDao);
        TreatmentService treatmentService = new TreatmentServiceImpl(treatmentDao);

        commands.put("noCommand", new NoCommand());
        commands.put("login", new LoginCommand(userService));
        commands.put("viewSignUp", new ViewSignUpCommand(doctorService));
        commands.put("viewWorkers", new ViewWorkersCommand(doctorService, patientService));
        commands.put("sortDoctors", new SortDoctorsCommand(doctorService));
        commands.put("sortPatients", new SortPatientsCommand(patientService));
        commands.put("viewSettings", new ViewSettingsCommand());
        commands.put("logout", new LogoutCommand());
        commands.put("viewCard", new ViewCardCommand(doctorService, patientService, dischargeService));
        commands.put("createCard", new CreateCardCommand(cardService, treatmentService, doctorService, patientService));
        commands.put("viewAppointment", new ViewAppointmentCommand(doctorService, patientService, treatmentService, cardService));
        commands.put("createAppointment", new CreateAppointmentCommand(cardService, doctorService, patientService, treatmentService));
        commands.put("discharge", new DischargeCommand(patientService, dischargeService, doctorService, cardService, treatmentService));
        commands.put("createDoctor", new CreateDoctorCommand(doctorService, nurseService, userService));
        commands.put("createPatient", new CreatePatientCommand(patientService));
        commands.put("setLocale", new ChangeLocaleCommand(userService));
    }

    /**
     * Return command object with the given name
     *
     * @param command Name of command
     * @return Command object
     */
    public static Command get(String command) {
        if (isNull(command) || !commands.containsKey(command)) {
            log.warn("Command not exists");
            return commands.get("noCommand");
        }
        return commands.get(command);
    }
}
