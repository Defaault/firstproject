<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ctg" uri="footer" %>
<html>
<head>
    <title>Login</title>
    <link rel="stylesheet" href="style/login.css">
    <link rel="stylesheet" href="style/general.css">
</head>
<body>
    <c:if test="${not empty param.lc}">
        <fmt:setLocale value="${param.lc}"/>
    </c:if>
    <div class="root">
            <article class="container">
                <div class="form">
                    <form action="/controller" method="post">
                    <label class="welcome"><fmt:message key="login.welcome"/></label>
                        <input type="hidden" name="command" value="login"/>
                        <input type="text" name="login" placeholder='<fmt:message key="login.login"/>'/>
                        <input type="password" name="password" placeholder='<fmt:message key="login.password"/>'/>
                        <input type="submit" value='<fmt:message key="login.submit"/>'/>
                    </form>
                    <c:if test="${not empty login_errors}">
                        <div class="errors">
                            <c:forEach items="${login_errors}" var="error">
                                <fmt:message key="${error.value}"/><br>
                            </c:forEach>
                        </div>
                        <c:remove var="login_errors" scope="session"/>
                    </c:if>
                </div>
            </article>
            <footer class="footer">
                <div class="language_container">
                    <fmt:message key="login.language"/>
                    <select class="select" onchange="self.location = '?lc='+this.options[this.selectedIndex].value">
                        <c:if test="${not empty param.lc}">
                            <option value="${param.lc}">${param.lc}</option>
                        </c:if>
                        <c:forEach items="${locales}" var="locale">
                                <c:if test="${locale != param.lc}">
                                <option value="${locale}">${locale}</option>
                                </c:if>
                        </c:forEach>
                    </select>
                </div>
                <div class="description">
                    <ctg:footer><fmt:message key="login.footer"/></ctg:footer>
                </div>
            </footer>
    </div>
</body>
</html>
