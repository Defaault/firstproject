<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>Sign Up</title>
    <link rel="stylesheet" href="style/signup.css">
    <link rel="stylesheet" href="style/general.css">
    <script type="text/javascript" src="js/select.js"></script>
    <script type="text/javascript" src="js/signup.js"></script>
</head>
<body onload="enabled()">
<%@ include file="/WEB-INF/jspf/header.jspf" %>
    <div class="root">
        <div class="main">
              <article class="doctor">
                  <div class="form">
                      <div class="h1">
                          <label ><fmt:message key="signup.form"/></label>
                      </div>
                      <form action="/controller" method="post">
                          <input type="hidden" name="command" value="createDoctor"/>
                          <fmt:message key="signup.role"/>
                          <select class="select" name="role" id="role" onclick="select(this.form)">
                                <c:forEach items="${roles}" var="role">
                                    <option value="${fn:toLowerCase(role)}"><fmt:message key="${role.name}"/></option>
                                </c:forEach>
                          </select>
                          <label id="label"><fmt:message key="signup.specialty"/></label>
                          <select class="select" id="specialty" name="specialty">
                                <c:forEach items="${specializations}" var="specialty">
                                    <option value="${fn:toLowerCase(specialty)}"><fmt:message key="${specialty.name}"/></option>
                                </c:forEach>
                          </select><br>
                          <input class="login" type="text" name="login" placeholder='<fmt:message key="signup.login"/>'
                                pattern = "\w{3,20}" title='<fmt:message key="check.signup.login"/>' required/>
                          <input type="text" name="first_name" placeholder='<fmt:message key="signup.name"/>'
                                pattern = "\p{L}{3,20}" title='<fmt:message key="check.signup.name"/>' required/>
                          <input type="text" name="second_name" placeholder='<fmt:message key="signup.surname"/>'
                                pattern = "\p{L}{3,20}" title='<fmt:message key="check.signup.surname"/>' required/>
                          <input type="password" name="password" placeholder='<fmt:message key="signup.password"/>'
                                pattern = "\w{6,20}" title='<fmt:message key="check.signup.password"/>' required/>
                          <input type="password" name="repeat_password" placeholder='<fmt:message key="signup.repeatPassword"/>'
                                title='<fmt:message key="check.signup.fill"/>' required/>
                          <input type="submit" value='<fmt:message key="signup.signupDoctor"/>'/>
                      </form>
                      <c:if test="${not empty signup_doctor_errors}">
                          <div class="errors">
                              <c:forEach items="${signup_doctor_errors}" var="error">
                                    <fmt:message key="${error.value}"/></br>
                              </c:forEach>
                          </div>
                          <c:remove var="signup_doctor_errors" scope="session"/>
                      </c:if>
                      <c:if test="${not empty statusDoctor}">
                            <div class="status">
                                <fmt:message key="${statusDoctor}"/>
                            </div>
                            <c:remove var="statusDoctor" scope="session"/>
                      </c:if>
                  </div>
              </article>
              <article class="separator">
                    <div class="firstLine">
                    </div>
                    <div class="middleText">
                        <fmt:message key="signup.or"/>
                    </div>
                    <div class="secondLine">
                    </div>
              </article>
              <article class="patient">
                    <div class="form">
                        <div class="h1">
                          <label class="label"><fmt:message key="signup.formPatient"/></label>
                        </div>
                        <form action="/controller" method="post">
                            <input type="hidden" name="command" value="createPatient"/>
                            <input type="text" name="first_name" placeholder='<fmt:message key="signup.name"/>'
                                  pattern = "\p{L}{3,20}" title='<fmt:message key="check.signup.name"/>' required/>
                            <input type="text" name="second_name" placeholder='<fmt:message key="signup.surname"/>'
                                  pattern = "\p{L}{3,20}" title='<fmt:message key="check.signup.surname"/>' required/>
                            <div class="formText"><fmt:message key="signup.birthday"/></div>
                            <input type="date" name="birthday" placeholder="date" title='<fmt:message key="check.signup.fill"/>' required/><br>
                            <div class="checkboxText">
                                <fmt:message key="signup.random"/>
                                <input id="checkbox" type="checkbox" name="random" value="yes" onclick="checkboxEnabled()"/>
                            </div>
                            <div class="formText">
                                <fmt:message key="signup.selectSpecialty"/>
                                <select id="random" class="select" name="selectRandom">
                                    <c:forEach items="${specializations}" var="specialty">
                                        <option value="${fn:toLowerCase(specialty)}"><fmt:message key="${specialty.name}"/></option>
                                    </c:forEach>
                                    <option value="all"><fmt:message key="specialty.all"/></option>
                                </select>
                            </div>
                            <div class="formText">
                                <fmt:message key="signup.selectDoctor"/>
                                <select id="select" class="select" name="doctor_id">
                                    <c:forEach items="${doctors}" var="doctor">
                                        <option value="${doctor.id}">${doctor.name} ${doctor.surname} (<fmt:message key="${doctor.specialtyEnum.name}"/>)</option>
                                    </c:forEach>
                                </select>
                                <c:if test="${empty doctors}">
                                    <label class="errorLabel"><fmt:message key="signup.noDoctors"/></label>
                                </c:if>
                            </div>

                            <input id="submit" type="submit" value='<fmt:message key="signup.signupPatient"/>'/>
                        </form>
                        <c:if test="${not empty signup_patient_errors}">
                              <div class="errors">
                                    <c:forEach items="${signup_patient_errors}" var="error">
                                        <fmt:message key="${error.value}"/></br>
                                    </c:forEach>
                              </div>
                              <c:remove var="signup_patient_errors" scope="session"/>
                        </c:if>
                        <c:if test="${not empty statusPatient}">
                            <div class="status">
                                <fmt:message key="${statusPatient}"/>
                            </div>
                            <c:remove var="statusPatient" scope="session"/>
                        </c:if>
                    </div>
              </article>
        </div>
        <%@ include file="/WEB-INF/jspf/footer.jspf" %>
  </div>
</body>
</html>