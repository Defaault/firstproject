package ua.nure.ipatov.summarytask.dao.impl;

import org.apache.log4j.Logger;
import ua.nure.ipatov.summarytask.constant.TreatmentEnum;
import ua.nure.ipatov.summarytask.dao.TreatmentDao;
import ua.nure.ipatov.summarytask.entity.Treatment;
import ua.nure.ipatov.summarytask.util.DBManagerUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static ua.nure.ipatov.summarytask.constant.Query.*;

public class TreatmentDaoImpl implements TreatmentDao {

    private static final Logger log = Logger.getLogger(TreatmentDaoImpl.class);

    @Override
    public void save(Treatment treatment) {

        try (Connection connection = DBManagerUtil.getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SAVE_TREATMENTS)) {

            preparedStatement.setString(1, treatment.getDescription());
            preparedStatement.setInt(2, treatment.getTypeOfTreatmentsId().getId());
            preparedStatement.setInt(3, treatment.getHospitalCardId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void deleteAllByCardId(int id) {

        try (Connection connection = DBManagerUtil.getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(DELETE_ALL_BY_CARD_ID)) {

            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            log.error(e.getMessage());
        }

    }

    @Override
    public List<Treatment> findTreatmentsByPatientId(int id) {

        List<Treatment> list = new ArrayList<>();

        try (Connection connection = DBManagerUtil.getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(FIND_TREATMENTS_BY_PATIENT_ID)) {

            preparedStatement.setInt(1, id);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {

                while (resultSet.next()) {
                    Treatment treatment = new Treatment();
                    treatment.setId(resultSet.getInt(1));
                    treatment.setDescription(resultSet.getString(2));
                    treatment.setHospitalCardId(resultSet.getInt(4));
                    treatment.setTypeOfTreatmentsId(TreatmentEnum.valueOf(resultSet.getString(5)));
                    list.add(treatment);
                }
                return list;
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return null;
    }
}
