package ua.nure.ipatov.summarytask.entity;

import ua.nure.ipatov.summarytask.constant.RoleEnum;

import static java.lang.System.lineSeparator;

/**
 * User entity.
 *
 * @author Artem Ipatov
 */
public class User {

    private int id;
    private String login;
    private String password;
    private String locale;
    private RoleEnum roleEnum;

    public User() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public RoleEnum getRoleEnum() {
        return roleEnum;
    }

    public void setRoleEnum(RoleEnum roleEnum) {
        this.roleEnum = roleEnum;
    }

    @Override
    public String toString() {
        return "User{" + lineSeparator()
                + "    id: " + id + lineSeparator()
                + "    login: " + login + lineSeparator()
                + "    locale: " + locale + lineSeparator()
                + "}";
    }
}
