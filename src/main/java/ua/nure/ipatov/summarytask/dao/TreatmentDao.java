package ua.nure.ipatov.summarytask.dao;

import ua.nure.ipatov.summarytask.entity.Treatment;

import java.util.List;

/**
 * Data access object for Treatment entity
 */
public interface TreatmentDao {

    /**
     * Save treatment entity.
     *
     * @param treatment treatment to save.
     */
    void save(Treatment treatment);

    /**
     * Delete all treatments by card id.
     *
     * @param id card id.
     */
    void deleteAllByCardId(int id);

    /**
     * Find all treatments by patient id.
     *
     * @param id patient id.
     * @return list of patients entities.
     */
    List<Treatment> findTreatmentsByPatientId(int id);
}
