package ua.nure.ipatov.summarytask.command;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static ua.nure.ipatov.summarytask.util.ConfigurationUtil.getConfigProperty;

/**
 * No command.
 *
 * @author Artem Ipatov
 */
public class NoCommand extends Command {

    private static final Logger log = Logger.getLogger(NoCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        log.debug("Command starts");

        log.debug("Command finished");
        return getConfigProperty("path.page.error");
    }
}
