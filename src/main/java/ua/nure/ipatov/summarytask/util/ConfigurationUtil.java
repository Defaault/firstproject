package ua.nure.ipatov.summarytask.util;

import java.util.ResourceBundle;

/**
 * Access to config.properties. Holder of path to jsp.
 *
 * @author Artem Ipatov
 */
public final class ConfigurationUtil {

    private ConfigurationUtil() {
    }

    /**
     * Get config property
     *
     * @param path path(key) of property.
     * @return String of path to jsp.
     */
    public static String getConfigProperty(String path) {
        ResourceBundle resourceBundle = ResourceBundle.getBundle("config");
        return resourceBundle.getString(path);
    }
}
