package ua.nure.ipatov.summarytask.constant;

/**
 * Locale constants.
 *
 * @author Artem Ipatov
 */
public enum LocaleEnum {
    EN("locale.en"),
    RU("locale.ru");

    LocaleEnum(String name) {
        this.name = name;
    }

    private String name;

    public String getName() {
        return name;
    }
}
