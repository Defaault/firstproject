package ua.nure.ipatov.summarytask.util;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

import static org.junit.Assert.assertEquals;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ResourceBundle.class, ConfigurationUtil.class})
public class ConfigurationUtilTest {

    @Mock
    private ResourceBundle resourceBundle;

    @Test
    public void getConfigPropertyTest() {
        mockStatic(ResourceBundle.class);

        PowerMockito.when(ResourceBundle.getBundle("config")).thenReturn(resourceBundle);
        when(resourceBundle.getString("key")).thenReturn("value");

        assertEquals("value", ConfigurationUtil.getConfigProperty("key"));
    }

    @Test(expected = MissingResourceException.class)
    public void notFindProperty() {
        ConfigurationUtil.getConfigProperty("qwe");
    }
}
