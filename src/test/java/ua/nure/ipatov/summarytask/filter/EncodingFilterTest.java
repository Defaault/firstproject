package ua.nure.ipatov.summarytask.filter;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
public class EncodingFilterTest {

    @InjectMocks
    private EncodingFilter encodingFilter;

    @Mock
    private HttpServletRequest httpServletRequest;

    @Mock
    private HttpServletResponse httpServletResponse;

    @Mock
    private FilterChain filterChain;

    @Test
    public void changeCharacterEncoding() throws IOException, ServletException {

        Whitebox.setInternalState(encodingFilter, "code", "UTF-8");
        when(httpServletRequest.getCharacterEncoding()).thenReturn("CP1251");

        encodingFilter.doFilter(httpServletRequest, httpServletResponse, filterChain);

        verify(httpServletRequest).setCharacterEncoding("UTF-8");
    }

    @Test
    public void doFilterTest() throws IOException, ServletException {

        encodingFilter.doFilter(httpServletRequest, httpServletResponse, filterChain);

        verify(filterChain).doFilter(httpServletRequest, httpServletResponse);
    }
}
