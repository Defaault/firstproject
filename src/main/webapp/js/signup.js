function enabled() {

   var input = document.getElementById("select");

   if (input.length == 0) {
        document.getElementById("select").disabled = true;
        document.getElementById("submit").disabled = true;
   }

   document.getElementById("random").disabled = true;
}

function checkboxEnabled(){

    var checkbox = document.getElementById("checkbox");

    if(checkbox.checked == true) {
        document.getElementById("random").disabled = false
        document.getElementById("select").disabled = true;
    } else {
        document.getElementById("select").disabled = false;
        document.getElementById("random").disabled = true;
    }
}