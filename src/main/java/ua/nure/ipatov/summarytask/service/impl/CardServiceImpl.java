package ua.nure.ipatov.summarytask.service.impl;

import ua.nure.ipatov.summarytask.dao.CardDao;
import ua.nure.ipatov.summarytask.entity.Card;
import ua.nure.ipatov.summarytask.service.CardService;

public class CardServiceImpl implements CardService {

    private CardDao cardDao;

    public CardServiceImpl(CardDao cardDao) {
        this.cardDao = cardDao;
    }

    @Override
    public int create(Card card) {
        return cardDao.save(card);
    }

    @Override
    public void deleteAllByPatientId(int id) {
        cardDao.deleteAllByPatientId(id);
    }

    @Override
    public int getCardIdByPatientId(int id) {
        return cardDao.findCardIdByPatientId(id);
    }

    @Override
    public Card getCardByPatientId(int id) {
        return cardDao.findByPatientId(id);
    }
}
