package ua.nure.ipatov.summarytask.service.impl;

import org.apache.log4j.Logger;
import ua.nure.ipatov.summarytask.dao.DischargeDao;
import ua.nure.ipatov.summarytask.dao.DoctorDao;
import ua.nure.ipatov.summarytask.dao.PatientDao;
import ua.nure.ipatov.summarytask.entity.Discharge;
import ua.nure.ipatov.summarytask.entity.Treatment;
import ua.nure.ipatov.summarytask.service.DischargeService;
import ua.nure.ipatov.summarytask.util.DBManagerUtil;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static java.lang.System.lineSeparator;

public class DischargeServiceImpl implements DischargeService {

    private DischargeDao dischargeDao;
    private DoctorDao doctorDao;
    private PatientDao patientDao;
    private static final Logger log = Logger.getLogger(DischargeServiceImpl.class);

    public DischargeServiceImpl(DischargeDao dischargeDao, DoctorDao doctorDao, PatientDao patientDao) {
        this.dischargeDao = dischargeDao;
        this.doctorDao = doctorDao;
        this.patientDao = patientDao;
    }

    @Override
    public void create(Discharge discharge, int patientId, int id) {
        try (Connection connection = DBManagerUtil.getInstance().getConnection()) {
            connection.setAutoCommit(false);
            patientDao.deleteById(patientId, connection);
            dischargeDao.save(discharge, connection);
            doctorDao.decrementCountPatients(id, connection);
            connection.commit();
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public List<Discharge> getDischarge(String name, String surname) {
        return dischargeDao.findByNameAndSurname(name, surname);
    }

    @Override
    public void saveFile(Discharge discharge, List<Treatment> treatments) {

        try (FileWriter fileWriter = new FileWriter(System.getProperty("user.dir") + "/"
                + discharge.getName() + "_" + discharge.getSurname() + ".txt");
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {

            bufferedWriter.append("DISCHARGE LIST")
                    .append(lineSeparator())
                    .append("Name: ")
                    .append(discharge.getName())
                    .append(lineSeparator())
                    .append("Surname: ")
                    .append(discharge.getSurname())
                    .append(lineSeparator())
                    .append("Birthday: ")
                    .append(discharge.getBirthday().toString())
                    .append(lineSeparator())
                    .append("Diagnosis: ")
                    .append(discharge.getDiagnosis())
                    .append(lineSeparator())
                    .append("Start of treatment: ")
                    .append(discharge.getStartOfTreatment().toString())
                    .append(lineSeparator())
                    .append("Treatments: ")
                    .append(lineSeparator());

            for (Treatment treatment : treatments) {
                bufferedWriter.append("-")
                        .append(treatment.getTypeOfTreatmentsId().toString())
                        .append(": ")
                        .append(treatment.getDescription())
                        .append(lineSeparator());
            }

            bufferedWriter.append("End of treatment: ")
                    .append(new Timestamp(System.currentTimeMillis()).toString());

        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public Duration calculateAllTime(List<Discharge> discharges) {

        long allTime = discharges.stream()
                .mapToLong((s) -> s.getEndOfTreatment().getTime() - s.getStartOfTreatment().getTime())
                .sum();

        return Duration.of(allTime, ChronoUnit.MILLIS);
    }
}
