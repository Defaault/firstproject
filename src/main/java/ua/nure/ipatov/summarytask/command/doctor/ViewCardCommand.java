package ua.nure.ipatov.summarytask.command.doctor;

import org.apache.log4j.Logger;
import ua.nure.ipatov.summarytask.command.Command;
import ua.nure.ipatov.summarytask.entity.Discharge;
import ua.nure.ipatov.summarytask.entity.Doctor;
import ua.nure.ipatov.summarytask.entity.Patient;
import ua.nure.ipatov.summarytask.entity.User;
import ua.nure.ipatov.summarytask.service.DischargeService;
import ua.nure.ipatov.summarytask.service.DoctorService;
import ua.nure.ipatov.summarytask.service.PatientService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.Duration;
import java.util.List;

import static java.util.Objects.nonNull;
import static ua.nure.ipatov.summarytask.util.ConfigurationUtil.getConfigProperty;

/**
 * View card command.
 *
 * @author Artem Ipatov
 */
public class ViewCardCommand extends Command {

    private DoctorService doctorService;
    private PatientService patientService;
    private DischargeService dischargeService;
    private static final Logger log = Logger.getLogger(ViewCardCommand.class);

    public ViewCardCommand(DoctorService doctorService, PatientService patientService, DischargeService dischargeService) {
        this.doctorService = doctorService;
        this.patientService = patientService;
        this.dischargeService = dischargeService;
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        log.debug("Command starts");

        HttpSession session = request.getSession();
        User user = (User) request.getSession().getAttribute("user");
        Doctor doctor = doctorService.getDoctorByUserId(user.getId());
        log.trace("Found doctor by user ID --> " + doctor);

        List<Patient> list = patientService.getPatientsWithoutCardByDoctorId(doctor.getId());
        log.trace("Found patients without card by doctor ID --> " + list);

        session.setAttribute("patients", list);
        log.trace("Set session attribute: patients --> " + list);

        session.removeAttribute("selected");
        log.trace("Delete session attribute: selected");

        String patientId = request.getParameter("patient");
        log.trace("Request parameter: patient --> " + patientId);

        if (nonNull(patientId) && !patientId.isEmpty()) {

            Patient patient = patientService.getPatientById(Integer.parseInt(patientId));
            log.trace("Found patient by id --> " + patient);

            session.setAttribute("selected", Integer.parseInt(patientId));
            log.trace("Set request attribute: selected --> " + patientId);

            List<Discharge> discharge = dischargeService.getDischarge(patient.getName(), patient.getSurname());
            log.trace("Found discharge --> " + discharge);

            request.setAttribute("history", discharge);
            log.trace("Set session attribute: history --> " + discharge);

            Duration time = dischargeService.calculateAllTime(discharge);

            session.setAttribute("time", time);

        }

        log.debug("Command finished");
        return getConfigProperty("path.page.doctor.start");
    }
}
