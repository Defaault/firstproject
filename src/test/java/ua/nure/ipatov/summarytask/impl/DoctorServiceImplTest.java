package ua.nure.ipatov.summarytask.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import ua.nure.ipatov.summarytask.constant.SpecialtyEnum;
import ua.nure.ipatov.summarytask.dao.DoctorDao;
import ua.nure.ipatov.summarytask.dao.UserDao;
import ua.nure.ipatov.summarytask.entity.Doctor;
import ua.nure.ipatov.summarytask.entity.dto.Checkin;
import ua.nure.ipatov.summarytask.service.impl.DoctorServiceImpl;
import ua.nure.ipatov.summarytask.util.DBManagerUtil;
import ua.nure.ipatov.summarytask.util.PasswordEncoderUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({PasswordEncoderUtil.class, DBManagerUtil.class})
public class DoctorServiceImplTest {

    @InjectMocks
    private DoctorServiceImpl doctorService;

    @Mock
    private Checkin checkin;

    @Mock
    private DoctorDao doctorDao;

    @Mock
    private UserDao userDao;

    @Mock
    private Connection connection;

    @Mock
    private DBManagerUtil dbManagerUtil;

    @Test
    public void alphabetSortTest() {
        Doctor doctor = new Doctor();
        Doctor secondDoctor = new Doctor();
        List<Doctor> list = new ArrayList<>();
        List<Doctor> result = new ArrayList<>();
        doctor.setName("Artem");
        secondDoctor.setName("Dima");

        result.add(doctor);
        result.add(secondDoctor);
        list.add(secondDoctor);
        list.add(doctor);

        doctorService.sort(list, "alphabet");

        assertEquals(result, list);
    }

    @Test
    public void patientSortTest() {
        Doctor doctor = new Doctor();
        Doctor secondDoctor = new Doctor();
        List<Doctor> list = new ArrayList<>();
        List<Doctor> result = new ArrayList<>();
        doctor.setPatientCount(1);
        secondDoctor.setPatientCount(2);

        result.add(doctor);
        result.add(secondDoctor);
        list.add(secondDoctor);
        list.add(doctor);

        doctorService.sort(list, "patient");

        assertEquals(result, list);
    }

    @Test
    public void specialtySortTest() {
        Doctor doctor = new Doctor();
        Doctor secondDoctor = new Doctor();
        List<Doctor> list = new ArrayList<>();
        List<Doctor> result = new ArrayList<>();
        doctor.setSpecialtyEnum(SpecialtyEnum.PEDIATRICIAN);
        secondDoctor.setSpecialtyEnum(SpecialtyEnum.SURGEON);

        result.add(doctor);
        result.add(secondDoctor);
        list.add(secondDoctor);
        list.add(doctor);

        doctorService.sort(list, "specialty");

        assertEquals(result, list);
    }

    @Test
    public void nullListSortTest() {
        doctorService.sort(null, "patient");

        assertNull(null);
    }

    @Test
    public void createTest() {
        mockStatic(PasswordEncoderUtil.class);
        mockStatic(DBManagerUtil.class);

        when(DBManagerUtil.getInstance()).thenReturn(dbManagerUtil);
        when(dbManagerUtil.getConnection()).thenReturn(connection);

        doctorService.create(checkin);

        verify(doctorDao).save(any(), any());
        verify(userDao).save(any(), any());
    }

    @Test
    public void errorCreateTest() throws SQLException {
        mockStatic(PasswordEncoderUtil.class);
        mockStatic(DBManagerUtil.class);

        when(DBManagerUtil.getInstance()).thenReturn(dbManagerUtil);
        when(dbManagerUtil.getConnection()).thenReturn(connection);
        doThrow(new SQLException()).when(connection).setAutoCommit(false);

        doctorService.create(checkin);

        verify(doctorDao, never()).save(any(), any());
        verify(userDao, never()).save(any(), any());
    }
}
