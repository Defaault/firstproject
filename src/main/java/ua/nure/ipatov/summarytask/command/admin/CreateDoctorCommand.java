package ua.nure.ipatov.summarytask.command.admin;

import org.apache.log4j.Logger;
import ua.nure.ipatov.summarytask.command.Command;
import ua.nure.ipatov.summarytask.constant.RoleEnum;
import ua.nure.ipatov.summarytask.constant.SpecialtyEnum;
import ua.nure.ipatov.summarytask.entity.Doctor;
import ua.nure.ipatov.summarytask.entity.dto.Checkin;
import ua.nure.ipatov.summarytask.service.DoctorService;
import ua.nure.ipatov.summarytask.service.NurseService;
import ua.nure.ipatov.summarytask.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import static java.util.Objects.nonNull;
import static ua.nure.ipatov.summarytask.constant.Constant.*;
import static ua.nure.ipatov.summarytask.util.ConfigurationUtil.getConfigProperty;
import static ua.nure.ipatov.summarytask.util.DataValidationUtil.validateDoctor;

/**
 * Create doctor command.
 *
 * @author Artem Ipatov
 */
public class CreateDoctorCommand extends Command {

    private DoctorService doctorService;
    private NurseService nurseService;
    private UserService userService;
    private static final Logger log = Logger.getLogger(CreateDoctorCommand.class);

    public CreateDoctorCommand(DoctorService doctorService, NurseService nurseService, UserService userService) {
        this.doctorService = doctorService;
        this.nurseService = nurseService;
        this.userService = userService;
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        log.debug("Command starts");

        Checkin checkin = new Checkin();
        HttpSession session = request.getSession();
        checkin.setRoleEnum(RoleEnum.valueOf(request.getParameter(ROLE).toUpperCase()));

        checkin.setLogin(request.getParameter(LOGIN));
        checkin.setName(request.getParameter(FIRST_NAME));
        checkin.setSurname(request.getParameter(SECOND_NAME));
        checkin.setPassword(request.getParameter(PASSWORD));
        checkin.setRepeatPassword(request.getParameter(REPEAT_PASSWORD));
        Map<String, String> message = validateDoctor(checkin);

        if (nonNull(userService.getUserByLogin(checkin.getLogin().toLowerCase()))) {
            log.debug("User already exists");

            message.put("existUser", "errors.signup.findUser");
        }

        if (message.size() == 0) {
            log.debug("Data is valid");

            if (checkin.getRoleEnum() == RoleEnum.DOCTOR) {
                checkin.setSpecialtyEnum(SpecialtyEnum.valueOf(request.getParameter(SPECIALTY).toUpperCase()));
                doctorService.create(checkin);
                log.debug("Created doctor");
            } else {
                nurseService.create(checkin);
                log.debug("Created nurse");
            }
            List<Doctor> doctors = doctorService.getAllDoctors();
            log.trace("Found all doctors --> " + doctors);

            session.setAttribute("doctors", doctors);
            log.trace("Set session attribute: doctors --> " + doctors);

            session.setAttribute("statusDoctor", "status.signup.created");
            log.trace("Set request attribute: statusDoctor");

            session.setAttribute("signup_doctor_errors", message);
            log.trace("Set request attribute: signup_doctor_errors");

            log.debug("Command finished");
            return getConfigProperty("path.redirect.admin.start");
        } else {
            session.setAttribute("signup_doctor_errors", message);
            log.trace("Set request attribute: signup_doctor_errors");

            log.debug("Command finished");
            return getConfigProperty("path.redirect.admin.start");
        }
    }
}
