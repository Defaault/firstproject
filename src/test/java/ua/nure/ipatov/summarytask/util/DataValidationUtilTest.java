package ua.nure.ipatov.summarytask.util;

import org.junit.Before;
import org.junit.Test;
import org.mockito.junit.MockitoJUnitRunner;
import org.powermock.core.classloader.annotations.PrepareForTest;
import ua.nure.ipatov.summarytask.entity.Patient;
import ua.nure.ipatov.summarytask.entity.User;
import ua.nure.ipatov.summarytask.entity.dto.Checkin;

import java.util.Map;

import static org.junit.Assert.assertEquals;

@PrepareForTest(MockitoJUnitRunner.class)
public class DataValidationUtilTest {

    private Checkin checkin;
    private Patient patient;
    private User user;

    @Before
    public void setUp() {
        checkin = new Checkin();
    }

    @Test
    public void validateFirstConditionDoctor() {
        checkin.setLogin("qw");
        checkin.setName("qweqweqe");
        checkin.setSurname("qwewqewqe");
        checkin.setPassword("qweqweqwe");
        checkin.setRepeatPassword("qweqweqwe");

        Map<String, String> errors = DataValidationUtil.validateDoctor(checkin);

        assertEquals(1, errors.size());
    }

    @Test
    public void validateSecondConditionDoctor() {
        checkin.setLogin("qwqwe");
        checkin.setName("qw");
        checkin.setSurname("qwewqewqe");
        checkin.setPassword("qweqweqwe");
        checkin.setRepeatPassword("qweqweqwe");

        Map<String, String> errors = DataValidationUtil.validateDoctor(checkin);

        assertEquals(1, errors.size());
    }

    @Test
    public void validateSecondConditionSecondParameterDoctor() {
        checkin.setLogin("qwqweqe");
        checkin.setName("qweqweqe");
        checkin.setSurname("qw");
        checkin.setPassword("qweqweqwe");
        checkin.setRepeatPassword("qweqweqwe");

        Map<String, String> errors = DataValidationUtil.validateDoctor(checkin);

        assertEquals(1, errors.size());
    }

    @Test
    public void validateThirdConditionDoctor() {
        checkin.setLogin("qwqweqwe");
        checkin.setName("qweqweqe");
        checkin.setSurname("qwewqewqe");
        checkin.setPassword("qweqw");
        checkin.setRepeatPassword("qweqw");

        Map<String, String> errors = DataValidationUtil.validateDoctor(checkin);

        assertEquals(1, errors.size());
    }

    @Test
    public void validateFourthConditionDoctor() {
        checkin.setLogin("qwqweqwe");
        checkin.setName("qweqweqe");
        checkin.setSurname("qwewqewqe");
        checkin.setPassword("qweqwqwe");
        checkin.setRepeatPassword("qweqwqweee");

        Map<String, String> errors = DataValidationUtil.validateDoctor(checkin);

        assertEquals(1, errors.size());
    }

    @Test
    public void validate2FirstConditionFirstAttributeDoctor() {
        checkin.setLogin("qw");
        checkin.setName("qw");
        checkin.setSurname("qwewqewqe");
        checkin.setPassword("qweqwe");
        checkin.setRepeatPassword("qweqwe");

        Map<String, String> errors = DataValidationUtil.validateDoctor(checkin);

        assertEquals(2, errors.size());
    }

    @Test
    public void validate2FirstConditionSecondAttributeDoctor() {
        checkin.setLogin("qw");
        checkin.setName("qwqqweewq");
        checkin.setSurname("q");
        checkin.setPassword("qweqwe");
        checkin.setRepeatPassword("qweqwe");

        Map<String, String> errors = DataValidationUtil.validateDoctor(checkin);

        assertEquals(2, errors.size());
    }

    @Test
    public void validate2SecondConditionFirstAttributeDoctor() {
        checkin.setLogin("qwqwewq");
        checkin.setName("qw");
        checkin.setSurname("qwewqewqe");
        checkin.setPassword("qweqw");
        checkin.setRepeatPassword("qweqw");

        Map<String, String> errors = DataValidationUtil.validateDoctor(checkin);

        assertEquals(2, errors.size());
    }

    @Test
    public void validate2SecondConditionSecondAttributeDoctor() {
        checkin.setLogin("qwqwewq");
        checkin.setName("qwqweqeqw");
        checkin.setSurname("qw");
        checkin.setPassword("qweqw");
        checkin.setRepeatPassword("qweqw");

        Map<String, String> errors = DataValidationUtil.validateDoctor(checkin);

        assertEquals(2, errors.size());
    }

    @Test
    public void validate2ThirdConditionDoctor() {
        checkin.setLogin("qqweqw");
        checkin.setName("qweqwe");
        checkin.setSurname("qwewqewqe");
        checkin.setPassword("qw");
        checkin.setRepeatPassword("q");

        Map<String, String> errors = DataValidationUtil.validateDoctor(checkin);

        assertEquals(2, errors.size());
    }

    @Test
    public void validate3FirstConditionFirstAttributeDoctor() {
        checkin.setLogin("qw");
        checkin.setName("qw");
        checkin.setSurname("qwewqewqe");
        checkin.setPassword("qweqw");
        checkin.setRepeatPassword("qweqw");

        Map<String, String> errors = DataValidationUtil.validateDoctor(checkin);

        assertEquals(3, errors.size());
    }

    @Test
    public void validate3FirstConditionSecondAttributeDoctor() {
        checkin.setLogin("qw");
        checkin.setName("qqweqwewqew");
        checkin.setSurname("q");
        checkin.setPassword("qweqw");
        checkin.setRepeatPassword("qweqw");

        Map<String, String> errors = DataValidationUtil.validateDoctor(checkin);

        assertEquals(3, errors.size());
    }

    @Test
    public void validate3SecondConditionFirstAttributeDoctor() {
        checkin.setLogin("qwqewqewq");
        checkin.setName("qw");
        checkin.setSurname("qwewqewqe");
        checkin.setPassword("qw");
        checkin.setRepeatPassword("q");

        Map<String, String> errors = DataValidationUtil.validateDoctor(checkin);

        assertEquals(3, errors.size());
    }

    @Test
    public void validate3SecondConditionSecondAttributeDoctor() {
        checkin.setLogin("qwqewqewq");
        checkin.setName("qwqwewqeeqwe");
        checkin.setSurname("q");
        checkin.setPassword("qw");
        checkin.setRepeatPassword("q");

        Map<String, String> errors = DataValidationUtil.validateDoctor(checkin);

        assertEquals(3, errors.size());
    }

    @Test
    public void validateAllConditionsFirstAttributeDoctor() {
        checkin.setLogin("qw");
        checkin.setName("qw");
        checkin.setSurname("qwewqewqe");
        checkin.setPassword("qw");
        checkin.setRepeatPassword("q");

        Map<String, String> errors = DataValidationUtil.validateDoctor(checkin);

        assertEquals(4, errors.size());
    }

    @Test
    public void validateAllConditionsSecondAttributeDoctor() {
        checkin.setLogin("qw");
        checkin.setName("qwqwewqewqewq");
        checkin.setSurname("q");
        checkin.setPassword("qw");
        checkin.setRepeatPassword("q");

        Map<String, String> errors = DataValidationUtil.validateDoctor(checkin);

        assertEquals(4, errors.size());
    }

    @Test
    public void validateNullErrorConditionsDoctor() {
        checkin.setLogin("qwqweqwe");
        checkin.setName("qwqweqwewq");
        checkin.setSurname("qwewqewqe");
        checkin.setPassword("qweqwe");
        checkin.setRepeatPassword("qweqwe");

        Map<String, String> errors = DataValidationUtil.validateDoctor(checkin);

        assertEquals(0, errors.size());
    }

    @Before
    public void setUpPatient() {
        patient = new Patient();
    }

    @Test
    public void validateErrorFirstAttributePatient() {
        patient.setName("qw");
        patient.setSurname("qweqwe");

        Map<String, String> errors = DataValidationUtil.validatePatient(patient);

        assertEquals(1, errors.size());
    }

    @Test
    public void validateErrorSecondAttributePatient() {
        patient.setName("qwqwewqe");
        patient.setSurname("qw");

        Map<String, String> errors = DataValidationUtil.validatePatient(patient);

        assertEquals(1, errors.size());
    }

    @Test
    public void validateNullErrorPatient() {
        patient.setName("qwqwewqe");
        patient.setSurname("qwewqeqwe");

        Map<String, String> errors = DataValidationUtil.validatePatient(patient);

        assertEquals(0, errors.size());
    }

    @Test
    public void validateFirstConditionUser() {

        Map<String, String> errors = DataValidationUtil.validateUser("qwe", user);

        assertEquals(1, errors.size());
    }

    @Test
    public void validateSecondConditionUser() {

        user = new User();
        Map<String, String> errors = DataValidationUtil.validateUser("qwe", user);

        assertEquals(1, errors.size());
    }

    @Test
    public void validateNullErrorsUser() {
        String password = PasswordEncoderUtil.encodePassword("qwe");
        user = new User();
        user.setPassword(password);

        Map<String, String> errors = DataValidationUtil.validateUser("qwe", user);

        assertEquals(0, errors.size());
    }
}
