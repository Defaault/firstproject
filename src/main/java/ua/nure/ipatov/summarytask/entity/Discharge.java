package ua.nure.ipatov.summarytask.entity;

import java.sql.Date;
import java.sql.Timestamp;

import static java.lang.System.lineSeparator;

/**
 * Discharge entity.
 *
 * @author Artem Ipatov
 */
public class Discharge {

    private int id;
    private String name;
    private String surname;
    private Date birthday;
    private String diagnosis;
    private Timestamp startOfTreatment;
    private Timestamp endOfTreatment;

    public Discharge() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public Timestamp getStartOfTreatment() {
        return startOfTreatment;
    }

    public void setStartOfTreatment(Timestamp startOfTreatment) {
        this.startOfTreatment = startOfTreatment;
    }

    public Timestamp getEndOfTreatment() {
        return endOfTreatment;
    }

    public void setEndOfTreatment(Timestamp endOfTreatment) {
        this.endOfTreatment = endOfTreatment;
    }

    @Override
    public String toString() {
        return "Discharge{" + lineSeparator()
                + "    id: " + id + lineSeparator()
                + "    name: " + name + lineSeparator()
                + "    surname: " + surname + lineSeparator()
                + "    birthday: " + birthday + lineSeparator()
                + "    diagnosis: " + diagnosis + lineSeparator()
                + "}";
    }
}
