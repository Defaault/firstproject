package ua.nure.ipatov.summarytask.service;

import ua.nure.ipatov.summarytask.entity.Treatment;

import java.util.List;

/**
 * Service for Treatment entity
 */
public interface TreatmentService {

    /**
     * Create treatment.
     *
     * @param treatment treatment to create.
     */
    void create(Treatment treatment);

    /**
     * Delete all treatments by card id.
     *
     * @param id card id.
     */
    void deleteAllByCardId(int id);

    /**
     * Get all treatments by patient id.
     *
     * @param id patient id.
     * @return list of treatment entities.
     */
    List<Treatment> getTreatmentsByPatientId(int id);
}
