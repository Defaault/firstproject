package ua.nure.ipatov.summarytask.constant;

/**
 * Constant holder (request parameter, pattern)
 *
 * @author Artem Ipatov
 */
public final class Constant {

    public static final String USER = "user";
    public static final String LOGIN = "login";
    public static final String PASSWORD = "password";
    public static final String FIRST_NAME = "first_name";
    public static final String SECOND_NAME = "second_name";
    public static final String REPEAT_PASSWORD = "repeat_password";
    public static final String SPECIALTY = "specialty";
    public static final String ROLE = "role";
    public static final String BIRTHDAY = "birthday";
    public static final String DOCTOR_ID = "doctor_id";
    public static final String SORT = "sort";
    public static final String ALPHABET = "alphabet";
    public static final String PATIENT = "patient";
    public static final String DIAGNOSIS = "diagnosis";
    public static final String TREATMENT = "treatment";
    public static final String DESCRIPTION = "description";
    public static final String LOCALE = "lc";
    public static final String NAME_PATTERN = "\\p{L}{3,20}";
    public static final String LOGIN_PATTERN = "\\w{3,20}";
    public static final String PASSWORD_PATTERN = "\\w{6,20}";

    private Constant() {
    }
}
