package ua.nure.ipatov.summarytask.constant;

/**
 * Sort constants.
 *
 * @author Artem Ipatov
 */
public enum SortEnum {
    BIRTHDAY("sort.birthday"),
    ALPHABET("sort.alphabet"),
    SPECIALTY("sort.specialty"),
    PATIENT("sort.patientCount");

    SortEnum(String name) {
        this.name = name;
    }

    private String name;

    public String getName() {
        return name;
    }
}
