package ua.nure.ipatov.summarytask.service;

import ua.nure.ipatov.summarytask.entity.Discharge;
import ua.nure.ipatov.summarytask.entity.Treatment;

import java.time.Duration;
import java.util.List;

/**
 * Service for Discharge entity
 */
public interface DischargeService {

    /**
     * Create discharge.
     *
     * @param discharge discharge entity.
     * @param id        doctor id.
     */
    void create(Discharge discharge, int patientId, int id);

    /**
     * Get all discharges by name and surname.
     *
     * @param name    patient name.
     * @param surname patient surname.
     * @return list of discharge entities.
     */
    List<Discharge> getDischarge(String name, String surname);

    /**
     * Save discharge file.
     *
     * @param discharge  discharge entity.
     * @param treatments list of treatments.
     */
    void saveFile(Discharge discharge, List<Treatment> treatments);

    Duration calculateAllTime(List<Discharge> discharges);
}
