package ua.nure.ipatov.summarytask.command.admin;

import org.apache.log4j.Logger;
import ua.nure.ipatov.summarytask.command.Command;
import ua.nure.ipatov.summarytask.entity.Doctor;
import ua.nure.ipatov.summarytask.service.DoctorService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

import static java.util.Objects.nonNull;
import static ua.nure.ipatov.summarytask.constant.Constant.SORT;
import static ua.nure.ipatov.summarytask.util.ConfigurationUtil.getConfigProperty;

/**
 * Sort doctors command.
 *
 * @author Artem Ipatov
 */
public class SortDoctorsCommand extends Command {

    private DoctorService doctorService;
    private static final Logger log = Logger.getLogger(SortDoctorsCommand.class);

    public SortDoctorsCommand(DoctorService doctorService) {
        this.doctorService = doctorService;
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        log.debug("Command starts");

        String sort = request.getParameter(SORT);
        log.trace("Request parameter: sort --> " + sort);

        HttpSession session = request.getSession();
        List<Doctor> list = doctorService.getAllDoctors();
        log.trace("Found all doctors --> " + list);

        session.setAttribute("doctors", list);
        log.trace("Set session attribute: doctors --> " + list);

        if (nonNull(sort)) {
            log.debug("Sort not null");

            list = doctorService.sort(list, sort);
            log.debug("Sorted doctor list");

            session.setAttribute("selectedDoctorSort", sort);
            log.trace("Set session attribute: selectedDoctorSort --> " + sort);

            session.setAttribute("doctors", list);
            log.trace("Set session attribute: doctors --> " + list);
        }

        log.debug("Command finished");
        return getConfigProperty("path.page.admin.workers");
    }
}
