package ua.nure.ipatov.summarytask.dao.impl;

import org.apache.log4j.Logger;
import ua.nure.ipatov.summarytask.dao.CardDao;
import ua.nure.ipatov.summarytask.entity.Card;
import ua.nure.ipatov.summarytask.util.DBManagerUtil;

import java.sql.*;

import static ua.nure.ipatov.summarytask.constant.Query.*;

public class CardDaoImpl implements CardDao {

    private static final Logger log = Logger.getLogger(CardDaoImpl.class);

    @Override
    public int save(Card card) {

        try (Connection connection = DBManagerUtil.getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(HOSPITAL_SAVE, Statement.RETURN_GENERATED_KEYS)) {

            preparedStatement.setString(1, card.getDiagnosis());
            preparedStatement.setInt(2, card.getPatientId());
            preparedStatement.executeUpdate();

            try (ResultSet resultSet = preparedStatement.getGeneratedKeys()) {
                if (resultSet.next()) {
                    return resultSet.getInt(1);
                }
            }

        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return 0;
    }

    @Override
    public int findCardIdByPatientId(int id) {

        try (Connection connection = DBManagerUtil.getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(FIND)) {

            preparedStatement.setInt(1, id);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {

                if (resultSet.next()) {
                    return resultSet.getInt(1);
                }
            }

        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return 0;
    }

    @Override
    public void deleteAllByPatientId(int id) {

        try (Connection connection = DBManagerUtil.getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(DELETE_ALL_BY_PATIENT_ID)) {

            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            log.error(e.getMessage());
        }

    }

    @Override
    public Card findByPatientId(int id) {

        try (Connection connection = DBManagerUtil.getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(FIND_CARDS_BY_PATIENT_ID)) {

            preparedStatement.setInt(1, id);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {

                if (resultSet.next()) {
                    Card card = new Card();
                    card.setId(resultSet.getInt(1));
                    card.setDiagnosis(resultSet.getString(2));
                    card.setStartOfTreatment(resultSet.getTimestamp(3));
                    card.setPatientId(resultSet.getInt(4));
                    return card;
                }
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return null;
    }
}
