package ua.nure.ipatov.summarytask.command;

import org.apache.log4j.Logger;
import ua.nure.ipatov.summarytask.constant.RoleEnum;
import ua.nure.ipatov.summarytask.entity.User;
import ua.nure.ipatov.summarytask.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.jstl.core.Config;
import java.io.IOException;
import java.util.Map;

import static ua.nure.ipatov.summarytask.constant.Constant.LOGIN;
import static ua.nure.ipatov.summarytask.constant.Constant.PASSWORD;
import static ua.nure.ipatov.summarytask.util.ConfigurationUtil.getConfigProperty;
import static ua.nure.ipatov.summarytask.util.DataValidationUtil.validateUser;
import static ua.nure.ipatov.summarytask.util.InitializeNavigationBarUtil.initialize;

/**
 * Login command.
 *
 * @author Artem Ipatov
 */
public class LoginCommand extends Command {

    private UserService userService;

    private static final Logger log = Logger.getLogger(LoginCommand.class);

    public LoginCommand(UserService userService) {
        this.userService = userService;
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        log.debug("Command starts");

        HttpSession session = request.getSession();

        String login = request.getParameter(LOGIN);
        log.trace("Request parameter: login --> " + login);

        String password = request.getParameter(PASSWORD);
        log.trace("Request parameter: password --> " + password);

        User user = userService.getUserByLogin(login.toLowerCase());
        log.trace("Found user by login --> " + user);

        Map<String, String> errors = validateUser(password, user);

        if (errors.size() == 0) {
            log.debug("Data is valid");

            String locale = user.getLocale();
            RoleEnum roleEnum = user.getRoleEnum();
            Config.set(session, "javax.servlet.jsp.jstl.fmt.locale", locale);
            session.setAttribute("user", user);
            log.trace("Set session attribute: user --> " + user);

            session.setAttribute("navBar", initialize(roleEnum));
            log.trace("Set session attribute: navBar for role --> " + roleEnum);

            switch (roleEnum) {
                case ADMIN:
                    log.debug("Command finished");

                    return getConfigProperty("path.redirect.admin.start");
                case DOCTOR:
                    log.debug("Command finished");

                    return getConfigProperty("path.redirect.doctor.start");
                case NURSE:
                    log.debug("Command finished");

                    return getConfigProperty("path.redirect.nurse.start");
                default:
                    throw new EnumConstantNotPresentException(RoleEnum.class, RoleEnum.class.getName());
            }
        } else {
            log.debug("Data is invalid");

            session.setAttribute("login_errors", errors);
            log.trace("Set request attribute: login_errors --> " + errors);

            log.debug("Command finished");
            return getConfigProperty("path.redirect.login");
        }
    }
}
