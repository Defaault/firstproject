package ua.nure.ipatov.summarytask.command;

import org.apache.log4j.Logger;
import ua.nure.ipatov.summarytask.constant.RoleEnum;
import ua.nure.ipatov.summarytask.entity.*;
import ua.nure.ipatov.summarytask.service.CardService;
import ua.nure.ipatov.summarytask.service.DoctorService;
import ua.nure.ipatov.summarytask.service.PatientService;
import ua.nure.ipatov.summarytask.service.TreatmentService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

import static java.util.Objects.nonNull;
import static ua.nure.ipatov.summarytask.util.ConfigurationUtil.getConfigProperty;

/**
 * View appointment command
 *
 * @author Artem Ipatov
 */
public class ViewAppointmentCommand extends Command {

    private static final Logger log = Logger.getLogger(ViewAppointmentCommand.class);

    private DoctorService doctorService;
    private PatientService patientService;
    private CardService cardService;
    private TreatmentService treatmentService;

    public ViewAppointmentCommand(DoctorService doctorService, PatientService patientService,
                                  TreatmentService treatmentService, CardService cardService) {
        this.doctorService = doctorService;
        this.patientService = patientService;
        this.treatmentService = treatmentService;
        this.cardService = cardService;
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        log.debug("Command starts");

        HttpSession session = request.getSession();
        User user = (User) request.getSession().getAttribute("user");
        Doctor doctor = doctorService.getDoctorByUserId(user.getId());
        log.trace("Found doctor by ID --> " + doctor);
        List<Patient> list;
        String page;

        String patientId = request.getParameter("patient");
        log.trace("Request parameter: patient --> " + patientId);

        session.removeAttribute("selected");
        log.trace("Delete session attribute: selected");

        if (nonNull(patientId) && !patientId.isEmpty()) {
            int id = Integer.parseInt(patientId);

            session.setAttribute("selected", id);
            log.trace("Set session attribute: selected --> " + id);

            Card card = cardService.getCardByPatientId(id);
            log.trace("Found card by patient ID --> " + card);

            session.setAttribute("card", card);
            log.trace("Set session attribute: card --> " + card);

            List<Treatment> treatment = treatmentService.getTreatmentsByPatientId(id);
            log.trace("Found treatments by patient ID --> " + treatment);

            session.setAttribute("treatments", treatment);
            log.trace("Set session attribute: treatments --> " + treatment);
        }

        if (user.getRoleEnum() == RoleEnum.DOCTOR) {
            log.trace("User has role --> " + user.getRoleEnum());

            list = patientService.getPatientsWithDiagnosisByDoctorId(doctor.getId());
            log.trace("Found patients with diagnosis by doctor ID --> " + list);

            page = getConfigProperty("path.page.doctor.appointment");
        } else {
            log.trace("User has role --> " + user.getRoleEnum());

            list = patientService.getPatientsWithDiagnosis();
            log.trace("Found patients with diagnosis --> " + list);

            page = getConfigProperty("path.page.nurse.start");
        }
        session.setAttribute("patientsWithDiagnosis", list);
        log.trace("Set session attribute: patientsWithDiagnosis --> " + list);

        log.debug("Command finished");
        return page;
    }
}
