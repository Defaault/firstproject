package ua.nure.ipatov.summarytask.service;

import ua.nure.ipatov.summarytask.entity.User;

/**
 * Service for User entity.
 */
public interface UserService {

    /**
     * Get user by login.
     *
     * @param login user login.
     * @return user entity.
     */
    User getUserByLogin(String login);

    /**
     * Update user locale.
     *
     * @param id     user id.
     * @param locale new locale to update.
     */
    void updateLocale(int id, String locale);
}
