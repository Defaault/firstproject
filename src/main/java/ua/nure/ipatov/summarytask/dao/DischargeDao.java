package ua.nure.ipatov.summarytask.dao;

import ua.nure.ipatov.summarytask.entity.Discharge;

import java.sql.Connection;
import java.util.List;

/**
 * Data access object for Discharge entity
 */
public interface DischargeDao {

    /**
     * Save discharge entity.
     *
     * @param discharge  discharge to save.
     * @param connection Db connection.
     */
    void save(Discharge discharge, Connection connection);

    /**
     * Find all discharges by name and surname.
     *
     * @param name    patient name.
     * @param surname patient surname.
     * @return list of discharge entities.
     */
    List<Discharge> findByNameAndSurname(String name, String surname);
}
