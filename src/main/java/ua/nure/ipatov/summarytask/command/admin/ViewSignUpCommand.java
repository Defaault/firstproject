package ua.nure.ipatov.summarytask.command.admin;

import org.apache.log4j.Logger;
import ua.nure.ipatov.summarytask.command.Command;
import ua.nure.ipatov.summarytask.entity.Doctor;
import ua.nure.ipatov.summarytask.service.DoctorService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

import static ua.nure.ipatov.summarytask.util.ConfigurationUtil.getConfigProperty;

/**
 * View sign up page command.
 *
 * @author Artem Ipatov
 */
public class ViewSignUpCommand extends Command {

    private DoctorService doctorService;
    private static final Logger log = Logger.getLogger(ViewSignUpCommand.class);

    public ViewSignUpCommand(DoctorService doctorService) {
        this.doctorService = doctorService;
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        log.debug("Command starts");

        HttpSession session = request.getSession();
        List<Doctor> doctors = doctorService.getAllDoctors();
        log.trace("Found all doctors --> " + doctors);

        session.setAttribute("doctors", doctors);
        log.trace("Set session attribute: doctors --> " + doctors);

        log.debug("Command finished");
        return getConfigProperty("path.page.admin.start");
    }
}
