package ua.nure.ipatov.summarytask.dao.impl;

import org.apache.log4j.Logger;
import ua.nure.ipatov.summarytask.constant.SpecialtyEnum;
import ua.nure.ipatov.summarytask.dao.DoctorDao;
import ua.nure.ipatov.summarytask.entity.Doctor;
import ua.nure.ipatov.summarytask.util.DBManagerUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static ua.nure.ipatov.summarytask.constant.Query.*;

public class DoctorDaoImpl implements DoctorDao {

    private static final Logger log = Logger.getLogger(DoctorDaoImpl.class);

    @Override
    public void save(Doctor doctor, Connection connection) {

        try (PreparedStatement preparedStatement = connection.prepareStatement(DOCTORS_SAVE)) {

            preparedStatement.setString(1, doctor.getName());
            preparedStatement.setString(2, doctor.getSurname());
            preparedStatement.setInt(3, doctor.getPatientCount());
            preparedStatement.setInt(4, doctor.getSpecialtyEnum().getId());
            preparedStatement.setInt(5, doctor.getUserId());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            log.error(e.getMessage());

            try {
                connection.rollback();
            } catch (SQLException ex) {
                log.error(ex.getMessage());
            }
        }
    }

    @Override
    public List<Doctor> findAllDoctors() {

        List<Doctor> list = new ArrayList<>();

        try (Connection connection = DBManagerUtil.getInstance().getConnection();
             Statement statement = connection.createStatement()) {

            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {

                while (resultSet.next()) {
                    Doctor doctor = new Doctor();
                    doctor.setId(resultSet.getInt(1));
                    doctor.setName(resultSet.getString(2));
                    doctor.setSurname(resultSet.getString(3));
                    doctor.setPatientCount(resultSet.getInt(4));
                    doctor.setSpecialtyEnum(SpecialtyEnum.valueOf(resultSet.getString(7)));
                    list.add(doctor);
                }
                return list;
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return null;
    }

    @Override
    public void incrementCountPatients(int id, Connection connection) {

        try (PreparedStatement preparedStatement = connection.prepareStatement(INCREMENT_COUNT_PATIENTS)) {
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            log.error(e.getMessage());

            try {
                connection.rollback();
            } catch (SQLException ex) {
                log.error(ex.getMessage());
            }
        }
    }

    @Override
    public void decrementCountPatients(int id, Connection connection) {

        try (PreparedStatement preparedStatement = connection.prepareStatement(DECREMENT_COUNT_PATIENTS)) {
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            log.error(e.getMessage());

            try {
                connection.rollback();
            } catch (SQLException ex) {
                log.error(ex.getMessage());
            }
        }
    }

    @Override
    public Doctor findByUserId(int id) {

        try (Connection connection = DBManagerUtil.getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_USER_ID)) {

            preparedStatement.setInt(1, id);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {

                if (resultSet.next()) {
                    Doctor doctor = new Doctor();
                    doctor.setId(resultSet.getInt(1));
                    doctor.setName(resultSet.getString(2));
                    doctor.setSurname(resultSet.getString(3));
                    doctor.setPatientCount(resultSet.getInt(4));
                    doctor.setUserId(resultSet.getInt(6));
                    doctor.setSpecialtyEnum(SpecialtyEnum.valueOf(resultSet.getString(7)));
                    return doctor;
                }

            }

        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return null;
    }

    @Override
    public int findDoctorIdWithMinPatient() {

        try (Connection connection = DBManagerUtil.getInstance().getConnection();
             Statement statement = connection.createStatement()) {

            try (ResultSet resultSet = statement.executeQuery(FIND_DOCTOR_ID_WITH_MIN_PATIENT);) {

                if (resultSet.next()) {
                    return resultSet.getInt(1);
                }
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return 0;
    }

    @Override
    public int findDoctorIdBySpecialtyWithMinPatient(String selectRandom) {

        try (Connection connection = DBManagerUtil.getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(FIND_DOCTOR_ID_BY_SPECIALTY_WITH_MIN_PATIENT)) {

            preparedStatement.setString(1, selectRandom.toUpperCase());

            try (ResultSet resultSet = preparedStatement.executeQuery()) {

                if (resultSet.next()) {
                    return resultSet.getInt(1);
                }
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return 0;
    }
}
