package ua.nure.ipatov.summarytask.command.admin;

import org.apache.log4j.Logger;
import ua.nure.ipatov.summarytask.command.Command;
import ua.nure.ipatov.summarytask.entity.Patient;
import ua.nure.ipatov.summarytask.service.PatientService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

import static java.util.Objects.nonNull;
import static ua.nure.ipatov.summarytask.constant.Constant.SORT;
import static ua.nure.ipatov.summarytask.util.ConfigurationUtil.getConfigProperty;

/**
 * Sort patient command.
 *
 * @author Artem Ipatov
 */
public class SortPatientsCommand extends Command {

    private PatientService patientService;
    private static final Logger log = Logger.getLogger(SortPatientsCommand.class);

    public SortPatientsCommand(PatientService patientService) {
        this.patientService = patientService;
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        log.debug("Command starts");

        String sort = request.getParameter(SORT);
        log.trace("Request parameter: sort --> " + sort);

        List<Patient> list = patientService.getAllPatients();
        log.trace("Found all patients --> " + list);

        HttpSession session = request.getSession();
        session.setAttribute("patients", list);
        log.trace("Set session attribute: patients --> " + list);

        if (nonNull(sort)) {
            log.debug("Sort not null");

            list = patientService.sort(list, sort);
            log.debug("Sorted patient list");

            session.setAttribute("selectedPatientSort", sort);
            log.trace("Set session attribute: selectedPatientSort --> " + sort);

            session.setAttribute("patients", list);
            log.trace("Set session attribute: patients --> " + list);
        }

        log.debug("Command finished");
        return getConfigProperty("path.page.admin.workers");
    }
}
