package ua.nure.ipatov.summarytask.controller;

import org.apache.log4j.Logger;
import ua.nure.ipatov.summarytask.command.Command;
import ua.nure.ipatov.summarytask.constant.ActionType;
import ua.nure.ipatov.summarytask.container.CommandContainer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Main servlet controller
 *
 * @author Artem Ipatov
 */
public class Controller extends HttpServlet {

    private static final Logger log = Logger.getLogger(Controller.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        execute(req, resp, ActionType.GET);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        execute(req, resp, ActionType.POST);
    }

    /**
     * Main method of this controller
     */
    private void execute(HttpServletRequest req, HttpServletResponse resp, ActionType actionType) {

        log.debug("Controller starts");

        String path;
        String command = req.getParameter("command");
        log.trace("Request parameter: command --> " + command);

        Command container = CommandContainer.get(command);
        log.trace("Obtained command --> " + container);

        try {
            path = container.execute(req, resp);
            log.trace("Forward address --> " + path);

            switch (actionType) {
                case GET:
                    req.getRequestDispatcher(path).forward(req, resp);
                    log.trace("Forward to next page");
                    break;
                case POST:
                    resp.sendRedirect(path);
                    log.trace("Redirect to next page");
                    break;
            }
        } catch (IOException | ServletException e) {
            log.error(e.getMessage());
        }
        log.debug("Controller finished");
    }
}
