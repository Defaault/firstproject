package ua.nure.ipatov.summarytask.util;

import ua.nure.ipatov.summarytask.constant.RoleEnum;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Initialize of navigation bar.
 *
 * @author Artem Ipatov
 */
public final class InitializeNavigationBarUtil {

    private InitializeNavigationBarUtil() {
    }

    /**
     * Initialize of navigation bar.
     *
     * @param roleEnum user role.
     * @return map of navigation bar.
     */
    public static Map<String, String> initialize(RoleEnum roleEnum) {

        Map<String, String> map = new LinkedHashMap<>();

        switch (roleEnum) {
            case ADMIN:
                map.put("navBar.signup", "controller?command=viewSignUp");
                map.put("navBar.workers", "controller?command=viewWorkers");
                map.put("navBar.logout", "controller?command=logout");
                map.put("navBar.locale", "javascript:void(0)");
                return map;
            case DOCTOR:
                map.put("navBar.createCard", "controller?command=viewCard");
                map.put("navBar.appointment", "controller?command=viewAppointment");
                map.put("navBar.logout", "controller?command=logout");
                map.put("navBar.locale", "javascript:void(0)");
                return map;
            case NURSE:
                map.put("navBar.appointment", "controller?command=viewAppointment");
                map.put("navBar.logout", "controller?command=logout");
                map.put("navBar.locale", "javascript:void(0)");
                return map;
            default:
                throw new EnumConstantNotPresentException(RoleEnum.class, RoleEnum.class.getName());
        }
    }
}
