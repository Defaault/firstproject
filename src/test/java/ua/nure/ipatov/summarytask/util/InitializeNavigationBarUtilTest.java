package ua.nure.ipatov.summarytask.util;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import ua.nure.ipatov.summarytask.constant.RoleEnum;

import java.util.Map;

import static org.junit.Assert.assertEquals;

@RunWith(PowerMockRunner.class)
@PrepareForTest(RoleEnum.class)
public class InitializeNavigationBarUtilTest {

    @Test
    public void initializeAdminTest() {
        Map<String, String> result = InitializeNavigationBarUtil.initialize(RoleEnum.ADMIN);

        assertEquals(4, result.size());
    }

    @Test
    public void initializeDoctor() {
        Map<String, String> result = InitializeNavigationBarUtil.initialize(RoleEnum.DOCTOR);

        assertEquals(4, result.size());
    }

    @Test
    public void initializeNurse() {
        Map<String, String> result = InitializeNavigationBarUtil.initialize(RoleEnum.NURSE);

        assertEquals(3, result.size());
    }
}
