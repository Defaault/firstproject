package ua.nure.ipatov.summarytask.entity;

import static java.lang.System.lineSeparator;

/**
 * Nurse entity.
 *
 * @author Artem Ipatov
 */
public class Nurse {

    private int id;
    private String name;
    private String surname;
    private int userId;

    public Nurse() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "Nurse{" + lineSeparator()
                + "    id: " + id + lineSeparator()
                + "    name: " + name + lineSeparator()
                + "    surname: " + surname + lineSeparator()
                + "}";
    }
}
