package ua.nure.ipatov.summarytask.constant;

/**
 * Treatment constants.
 *
 * @author Artem Ipatov
 */
public enum TreatmentEnum {
    PROCEDURE(1, "card.procedure"),
    MEDICINE(2, "card.medicine"),
    OPERATION(3, "card.operation");

    TreatmentEnum(int id, String name) {
        this.id = id;
        this.name = name;
    }

    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
