package ua.nure.ipatov.summarytask.command.doctor;

import org.apache.log4j.Logger;
import ua.nure.ipatov.summarytask.command.Command;
import ua.nure.ipatov.summarytask.entity.*;
import ua.nure.ipatov.summarytask.service.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

import static ua.nure.ipatov.summarytask.constant.Constant.USER;
import static ua.nure.ipatov.summarytask.util.ConfigurationUtil.getConfigProperty;

/**
 * Discharge command.
 *
 * @author Artem Ipatov
 */
public class DischargeCommand extends Command {

    private PatientService patientService;
    private DischargeService dischargeService;
    private DoctorService doctorService;
    private CardService cardService;
    private TreatmentService treatmentService;
    private static final Logger log = Logger.getLogger(DischargeCommand.class);

    public DischargeCommand(PatientService patientService, DischargeService dischargeService,
                            DoctorService doctorService, CardService cardService, TreatmentService treatmentService) {
        this.patientService = patientService;
        this.dischargeService = dischargeService;
        this.doctorService = doctorService;
        this.cardService = cardService;
        this.treatmentService = treatmentService;
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        log.debug("Command starts");

        HttpSession session = request.getSession();
        int patientId = (int) session.getAttribute("selected");
        User user = (User) session.getAttribute(USER);
        Discharge discharge = new Discharge();

        String save = request.getParameter("file");
        log.trace("Request parameter: asve --> " + save);

        Patient patient = patientService.getPatientById(patientId);
        log.trace("Found patient by ID --> " + patient);

        Card card = cardService.getCardByPatientId(patient.getId());
        log.trace("Found card by patient id --> " + card);

        discharge.setName(patient.getName());
        discharge.setSurname(patient.getSurname());
        discharge.setBirthday(patient.getBirthday());
        discharge.setDiagnosis(card.getDiagnosis());
        discharge.setStartOfTreatment(card.getStartOfTreatment());

        Doctor doctor = doctorService.getDoctorByUserId(user.getId());
        log.trace("Found doctor by user id --> " + doctor);

        List<Treatment> treatments = treatmentService.getTreatmentsByPatientId(patientId);
        log.trace("Fount patient list of treatments --> " + treatments);

        dischargeService.create(discharge, patientId, doctor.getId());
        log.debug("Created discharge");

        log.trace("Saving file.........");
        if ("yes".equals(save)) {
            dischargeService.saveFile(discharge, treatments);
        }
        log.trace("File saved");

        List<Patient> list = patientService.getPatientsWithDiagnosisByDoctorId(doctor.getId());
        log.trace("Found patients with diagnosis by doctor id --> " + list);

        session.setAttribute("patientsWithDiagnosis", list);
        log.trace("Set session attribute: patientsWithDiagnosis --> " + list);

        log.debug("Command finished");
        return getConfigProperty("path.redirect.doctor.appointment");
    }
}
