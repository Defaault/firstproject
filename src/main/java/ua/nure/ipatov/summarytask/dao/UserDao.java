package ua.nure.ipatov.summarytask.dao;

import ua.nure.ipatov.summarytask.entity.User;

import java.sql.Connection;

/**
 * Data access object for User entity.
 */
public interface UserDao {

    /**
     * Save user entity.
     *
     * @param user       user to save.
     * @param connection Db connection.
     * @return user id.
     */
    int save(User user, Connection connection);

    /**
     * Find user by login.
     *
     * @param login user login.
     * @return user entity.
     */
    User findByLogin(String login);

    /**
     * Update user locale.
     *
     * @param id     user id.
     * @param locale new locale to set.
     */
    void updateLocale(int id, String locale);
}
