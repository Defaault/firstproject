package ua.nure.ipatov.summarytask.entity;

import ua.nure.ipatov.summarytask.constant.SpecialtyEnum;

import static java.lang.System.lineSeparator;

/**
 * Doctor entity.
 *
 * @author Artem Ipatov
 */
public class Doctor {

    private int id;
    private String name;
    private String surname;
    private int patientCount;
    private SpecialtyEnum specialtyEnum;
    private int userId;

    public Doctor() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getPatientCount() {
        return patientCount;
    }

    public void setPatientCount(int patientCount) {
        this.patientCount = patientCount;
    }

    public SpecialtyEnum getSpecialtyEnum() {
        return specialtyEnum;
    }

    public void setSpecialtyEnum(SpecialtyEnum specialtyEnum) {
        this.specialtyEnum = specialtyEnum;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "Doctor{" + lineSeparator()
                + "    id: " + id + lineSeparator()
                + "    name: " + name + lineSeparator()
                + "    surname: " + surname + lineSeparator()
                + "    specialty: " + specialtyEnum + lineSeparator()
                + "}";
    }
}
