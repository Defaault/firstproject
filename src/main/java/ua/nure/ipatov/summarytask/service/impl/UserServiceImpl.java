package ua.nure.ipatov.summarytask.service.impl;

import ua.nure.ipatov.summarytask.dao.UserDao;
import ua.nure.ipatov.summarytask.entity.User;
import ua.nure.ipatov.summarytask.service.UserService;

public class UserServiceImpl implements UserService {

    private UserDao userDao;

    public UserServiceImpl(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public User getUserByLogin(String login) {
        return userDao.findByLogin(login);
    }

    @Override
    public void updateLocale(int id, String locale) {
        userDao.updateLocale(id, locale);
    }
}
