package ua.nure.ipatov.summarytask.util;

import ua.nure.ipatov.summarytask.entity.Patient;
import ua.nure.ipatov.summarytask.entity.User;
import ua.nure.ipatov.summarytask.entity.dto.Checkin;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import static java.util.Objects.isNull;
import static ua.nure.ipatov.summarytask.constant.Constant.*;
import static ua.nure.ipatov.summarytask.util.PasswordEncoderUtil.matches;

/**
 * Data validator.
 *
 * @author Artem Ipatov
 */
public final class DataValidationUtil {

    private DataValidationUtil() {
    }

    /**
     * Validate doctor data.
     *
     * @param checkin data transfer object of doctor.
     * @return map of errors.
     */
    public static Map<String, String> validateDoctor(Checkin checkin) {
        Map<String, String> errors = new HashMap<>();

        if (!Pattern.matches(LOGIN_PATTERN, checkin.getLogin())) {
            errors.put("login", "errors.signup.login");
        }
        if (!Pattern.matches(NAME_PATTERN, checkin.getName()) || !Pattern.matches(NAME_PATTERN, checkin.getSurname())) {
            errors.put("name", "errors.signup.name");
        }
        if (!Pattern.matches(PASSWORD_PATTERN, checkin.getPassword())) {
            errors.put("password", "errors.signup.password");
        }
        if (!checkin.getPassword().equals(checkin.getRepeatPassword())) {
            errors.put("passwordMatch", "errors.signup.passwordMatch");
        }
        return errors;
    }

    /**
     * Validate patient data.
     *
     * @param patient patient entity.
     * @return map of errors.
     */
    public static Map<String, String> validatePatient(Patient patient) {
        Map<String, String> errors = new HashMap<>();

        if (!Pattern.matches(NAME_PATTERN, patient.getName()) || !Pattern.matches(NAME_PATTERN, patient.getSurname())) {
            errors.put("loginName", "errors.signup.patientNameSurname");
        }
        return errors;
    }

    /**
     * Validate user data.
     *
     * @param password user password.
     * @param user     user entity.
     * @return map of errors.
     */
    public static Map<String, String> validateUser(String password, User user) {
        Map<String, String> errors = new HashMap<>();

        if (isNull(user)) {
            errors.put("notFound", "errors.login.notFound");
        } else if (!matches(password, user.getPassword())) {
            errors.put("incorrectPassword", "errors.login.incorrectPassword");
        }
        return errors;
    }
}
