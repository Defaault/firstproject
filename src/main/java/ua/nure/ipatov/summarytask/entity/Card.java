package ua.nure.ipatov.summarytask.entity;

import java.sql.Timestamp;

import static java.lang.System.lineSeparator;

/**
 * Card entity.
 *
 * @author Artem Ipatov
 */
public class Card {

    private int id;
    private String diagnosis;
    private Timestamp startOfTreatment;
    private int patientId;

    public Card() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public Timestamp getStartOfTreatment() {
        return startOfTreatment;
    }

    public void setStartOfTreatment(Timestamp startOfTreatment) {
        this.startOfTreatment = startOfTreatment;
    }

    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    @Override
    public String toString() {
        return "Card{" + lineSeparator()
                + "    id: " + id + lineSeparator()
                + "    diagnosis: " + diagnosis + lineSeparator()
                + "}";
    }
}
