package ua.nure.ipatov.summarytask.service.impl;

import org.apache.log4j.Logger;
import ua.nure.ipatov.summarytask.dao.NurseDao;
import ua.nure.ipatov.summarytask.dao.UserDao;
import ua.nure.ipatov.summarytask.entity.Nurse;
import ua.nure.ipatov.summarytask.entity.User;
import ua.nure.ipatov.summarytask.entity.dto.Checkin;
import ua.nure.ipatov.summarytask.service.NurseService;
import ua.nure.ipatov.summarytask.util.DBManagerUtil;

import java.sql.Connection;
import java.sql.SQLException;

import static ua.nure.ipatov.summarytask.util.PasswordEncoderUtil.encodePassword;

public class NurseServiceImpl implements NurseService {

    private NurseDao nurseDao;
    private UserDao userDao;
    private static final Logger log = Logger.getLogger(NurseServiceImpl.class);

    public NurseServiceImpl(NurseDao nurseDao, UserDao userDao) {
        this.nurseDao = nurseDao;
        this.userDao = userDao;
    }

    @Override
    public void create(Checkin checkin) {
        Nurse nurse = new Nurse();
        User user = new User();

        user.setLogin(checkin.getLogin());
        user.setPassword(encodePassword(checkin.getPassword()));
        user.setRoleEnum(checkin.getRoleEnum());

        nurse.setName(checkin.getName());
        nurse.setSurname(checkin.getSurname());

        try (Connection connection = DBManagerUtil.getInstance().getConnection()) {
            connection.setAutoCommit(false);
            int id = userDao.save(user, connection);
            nurse.setUserId(id);
            nurseDao.save(nurse, connection);
            connection.commit();
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
    }
}
