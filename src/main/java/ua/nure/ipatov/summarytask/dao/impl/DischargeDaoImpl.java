package ua.nure.ipatov.summarytask.dao.impl;

import org.apache.log4j.Logger;
import ua.nure.ipatov.summarytask.dao.DischargeDao;
import ua.nure.ipatov.summarytask.entity.Discharge;
import ua.nure.ipatov.summarytask.util.DBManagerUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static ua.nure.ipatov.summarytask.constant.Query.DISCHARGE_SAVE;
import static ua.nure.ipatov.summarytask.constant.Query.FIND_DISCHARGE;

public class DischargeDaoImpl implements DischargeDao {

    private static final Logger log = Logger.getLogger(DischargeDaoImpl.class);

    @Override
    public void save(Discharge discharge, Connection connection) {

        try (PreparedStatement preparedStatement = connection.prepareStatement(DISCHARGE_SAVE)) {

            preparedStatement.setString(1, discharge.getName());
            preparedStatement.setString(2, discharge.getSurname());
            preparedStatement.setDate(3, discharge.getBirthday());
            preparedStatement.setString(4, discharge.getDiagnosis());
            preparedStatement.setTimestamp(5, discharge.getStartOfTreatment());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            log.error(e.getMessage());

            try {
                connection.rollback();
            } catch (SQLException ex) {
                log.error(ex.getMessage());
            }
        }
    }

    @Override
    public List<Discharge> findByNameAndSurname(String name, String surname) {

        List<Discharge> list = new ArrayList<>();

        try (Connection connection = DBManagerUtil.getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(FIND_DISCHARGE)) {

            preparedStatement.setString(1, name);
            preparedStatement.setString(2, surname);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {

                while (resultSet.next()) {
                    Discharge discharge = new Discharge();
                    discharge.setId(resultSet.getInt(1));
                    discharge.setName(resultSet.getString(2));
                    discharge.setSurname(resultSet.getString(3));
                    discharge.setBirthday(resultSet.getDate(4));
                    discharge.setDiagnosis(resultSet.getString(5));
                    discharge.setStartOfTreatment(resultSet.getTimestamp(6));
                    discharge.setEndOfTreatment(resultSet.getTimestamp(7));
                    list.add(discharge);
                }
                return list;
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return null;
    }
}
