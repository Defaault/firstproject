<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>Appointment</title>
    <link rel="stylesheet" href="style/card.css">
    <link rel="stylesheet" href="style/general.css">
    <script type="text/javascript" src="js/card.js"></script>
</head>
<body onload="enabled();">
    <%@ include file="/WEB-INF/jspf/header.jspf"%>
        <div class="root">
            <c:choose>
                <c:when test="${not empty selected}">
                    <article class="container">
                        <div class="main">
                            <div class="formContainer">
                                <form class="selectForm" action="/controller">
                                    <input type="hidden" name="command" value="viewAppointment"/>
                                    <div class="selectPatient">
                                        <fmt:message key="card.selectPatient"/>
                                            <select id="select" class="select" name="patient">
                                                <c:forEach items="${patientsWithDiagnosis}" var="patient">
                                                    <c:choose>
                                                        <c:when test="${patient.id eq selected}">
                                                            <option value="${patient.id}" selected>${patient.name}</option>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <option value="${patient.id}">${patient.name}</option>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </c:forEach>
                                            </select>
                                    </div>
                                    <input id="submit" type="submit" value='<fmt:message key="appointment.find"/>'/>
                                </form>
                            </div>
                            <div class="h1">
                                <label><fmt:message key="appointment.formForAppointment"/></label>
                            </div>
                            <form class="form" action="controller" method="post">
                                <input type="hidden" name="command" value="createAppointment"/>
                                <fmt:message key="appointment.diagnosis"/><input type="text" value="${card.diagnosis}" readonly/>  <br>
                                <fmt:message key="card.typeOfTreatment"/>
                                <select class="select" name="treatment">
                                      <c:forEach items="${nurseTreatments}" var="treatment">
                                        <option value="${fn:toLowerCase(treatment)}"><fmt:message key="${treatment.name}"/></option>
                                      </c:forEach>
                                </select><br>
                                <input type="text" name="description" placeHolder='<fmt:message key="card.description"/>' required/>
                                <input type="submit" value='<fmt:message key="appointment.createTreatment"/>' />
                            </form>
                        </div>
                    </article>
                    <article class="history">
                        <c:choose>
                            <c:when test="${not empty treatments}">
                                <div>
                                    <div class="h1">
                                        <label><fmt:message key="appointment.patientTreatmentHistory"/></label>
                                    </div>
                                    <div class="tableContainer">
                                        <table class="table">
                                            <tr>
                                                <th><fmt:message key="appointment.typeOfTreatment"/></th>
                                                <th><fmt:message key="appointment.description"/></th>
                                            </tr>
                                                <c:forEach items="${treatments}" var="var">
                                                    <tr>
                                                        <td><fmt:message key="${var.typeOfTreatmentsId.name}"/></td>
                                                        <td>${var.description}</td>
                                                    </tr>
                                                </c:forEach>
                                        </table>
                                    </div>
                                </div>
                            </c:when>
                            <c:otherwise>
                                    <div class="h1">
                                        <label><fmt:message key="appointment.noTreatmentHistory"/></label>
                                    </div>
                            </c:otherwise>
                        </c:choose>
                    </article>
                </c:when>
                <c:otherwise>
                    <div class="emptySelect">
                          <form class="selectForm" action="/controller">
                               <input type="hidden" name="command" value="viewAppointment"/>
                                    <div class="selectPatient">
                                        <fmt:message key="card.selectPatient"/>
                                        <select id="select" class="select" name="patient">
                                            <c:forEach items="${patientsWithDiagnosis}" var="patient">
                                                <c:choose>
                                                    <c:when test="${patient.id eq selected}">
                                                         <option value="${patient.id}" selected>${patient.name}</option>
                                                    </c:when>
                                                    <c:otherwise>
                                                          <option value="${patient.id}">${patient.name}</option>
                                                    </c:otherwise>
                                                </c:choose>
                                            </c:forEach>
                                        </select>
                                    <c:if test="${empty patientsWithDiagnosis}">
                                        <label class="errorLabel"><fmt:message key="card.noPatients"/></label>
                                    </c:if>
                                    </div>
                               <input id="submit" type="submit" value='<fmt:message key="appointment.find"/>'/>
                          </form>
                    </div>
                </c:otherwise>
            </c:choose>
        </div>
    <%@ include file="/WEB-INF/jspf/footer.jspf"%>
</body>
</html>