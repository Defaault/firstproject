package ua.nure.ipatov.summarytask.service;

import ua.nure.ipatov.summarytask.entity.dto.Checkin;

/**
 * Service for Nurse entity
 */
public interface NurseService {

    /**
     * Create nurse.
     *
     * @param checkin data transfer object of nurse
     */
    void create(Checkin checkin);
}
