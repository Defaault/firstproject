package ua.nure.ipatov.summarytask.command;

import org.apache.log4j.Logger;
import ua.nure.ipatov.summarytask.entity.User;
import ua.nure.ipatov.summarytask.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.jstl.core.Config;
import java.io.IOException;

import static ua.nure.ipatov.summarytask.constant.Constant.LOCALE;
import static ua.nure.ipatov.summarytask.util.ConfigurationUtil.getConfigProperty;

/**
 * Change locale command.
 *
 * @author Artem Ipatov
 */
public class ChangeLocaleCommand extends Command {

    private UserService userService;
    private static final Logger log = Logger.getLogger(ChangeLocaleCommand.class);

    public ChangeLocaleCommand(UserService userService) {
        this.userService = userService;
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        log.debug("Command starts");

        HttpSession session = request.getSession();
        String locale = request.getParameter(LOCALE);
        log.trace("Request parameter: locale --> " + locale);

        Config.set(session, "javax.servlet.jsp.jstl.fmt.locale", locale);
        User user = (User) session.getAttribute("user");
        userService.updateLocale(user.getId(), locale);
        log.debug("Locale has been updated");

        log.debug("Command finished");
        return getConfigProperty("path.page." + user.getRoleEnum().toString().toLowerCase() + ".start");
    }
}
