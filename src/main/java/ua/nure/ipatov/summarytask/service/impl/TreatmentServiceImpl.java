package ua.nure.ipatov.summarytask.service.impl;

import ua.nure.ipatov.summarytask.dao.TreatmentDao;
import ua.nure.ipatov.summarytask.entity.Treatment;
import ua.nure.ipatov.summarytask.service.TreatmentService;

import java.util.List;

public class TreatmentServiceImpl implements TreatmentService {

    private TreatmentDao treatmentDao;

    public TreatmentServiceImpl(TreatmentDao treatmentDao) {
        this.treatmentDao = treatmentDao;
    }

    @Override
    public void create(Treatment treatment) {
        treatmentDao.save(treatment);
    }

    @Override
    public void deleteAllByCardId(int id) {
        treatmentDao.deleteAllByCardId(id);
    }

    @Override
    public List<Treatment> getTreatmentsByPatientId(int id) {
        return treatmentDao.findTreatmentsByPatientId(id);
    }
}
