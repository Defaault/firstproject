package ua.nure.ipatov.summarytask.service;

import ua.nure.ipatov.summarytask.entity.Card;

/**
 * Service for Card entity
 */
public interface CardService {

    /**
     * Create card.
     *
     * @param card card to save.
     * @return card id.
     */
    int create(Card card);

    /**
     * Get card id by patient id.
     *
     * @param id patient id.
     * @return card id.
     */
    int getCardIdByPatientId(int id);

    /**
     * Get card by patient id.
     *
     * @param id patient id.
     * @return card entity.
     */
    Card getCardByPatientId(int id);

    /**
     * Delete all cards by patient id.
     *
     * @param id patient id.
     */
    void deleteAllByPatientId(int id);
}
