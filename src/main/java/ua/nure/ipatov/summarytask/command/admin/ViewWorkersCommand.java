package ua.nure.ipatov.summarytask.command.admin;

import org.apache.log4j.Logger;
import ua.nure.ipatov.summarytask.command.Command;
import ua.nure.ipatov.summarytask.constant.SortEnum;
import ua.nure.ipatov.summarytask.entity.Doctor;
import ua.nure.ipatov.summarytask.entity.Patient;
import ua.nure.ipatov.summarytask.service.DoctorService;
import ua.nure.ipatov.summarytask.service.PatientService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.EnumSet;
import java.util.List;

import static ua.nure.ipatov.summarytask.util.ConfigurationUtil.getConfigProperty;

/**
 * View doctors and patients command.
 *
 * @author Artem Ipatov
 */
public class ViewWorkersCommand extends Command {

    private DoctorService doctorService;
    private PatientService patientService;
    private static final Logger log = Logger.getLogger(ViewWorkersCommand.class);

    public ViewWorkersCommand(DoctorService doctorService, PatientService patientService) {
        this.doctorService = doctorService;
        this.patientService = patientService;
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        log.debug("Command starts");

        HttpSession session = request.getSession();
        List<Doctor> doctors = doctorService.getAllDoctors();
        log.trace("Found all doctors --> " + doctors);

        List<Patient> patients = patientService.getAllPatients();
        log.trace("Found all patients --> " + patients);

        EnumSet<SortEnum> sortDoctorList = EnumSet.range(SortEnum.ALPHABET, SortEnum.PATIENT);
        EnumSet<SortEnum> sortPatientList = EnumSet.range(SortEnum.BIRTHDAY, SortEnum.ALPHABET);

        session.removeAttribute("selectedDoctorSort");
        session.removeAttribute("selectedPatientSort");
        session.setAttribute("sortDoctorList", sortDoctorList);
        log.trace("Set session attribute: sortDoctorList --> " + sortDoctorList);

        session.setAttribute("sortPatientList", sortPatientList);
        log.trace("Set session attribute: sortPatientList --> " + sortPatientList);

        session.setAttribute("doctors", doctors);
        log.trace("Set session attribute: doctors --> " + doctors);

        session.setAttribute("patients", patients);
        log.trace("Set session attribute: patients --> " + patients);

        log.debug("Command finished");
        return getConfigProperty("path.page.admin.workers");
    }
}
