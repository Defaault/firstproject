package ua.nure.ipatov.summarytask.command;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static ua.nure.ipatov.summarytask.util.ConfigurationUtil.getConfigProperty;

/**
 * Logout command.
 *
 * @author Artem Ipatov
 */
public class LogoutCommand extends Command {

    private static final Logger log = Logger.getLogger(LogoutCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        log.debug("Command starts");

        HttpSession session = request.getSession();
        session.invalidate();
        log.debug("Session invalidated");

        log.debug("Command finished");
        return getConfigProperty("path.page.login");
    }
}
