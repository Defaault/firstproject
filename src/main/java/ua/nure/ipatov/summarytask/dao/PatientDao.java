package ua.nure.ipatov.summarytask.dao;

import ua.nure.ipatov.summarytask.entity.Patient;

import java.sql.Connection;
import java.util.List;

/**
 * Data access object for Patient entity
 */
public interface PatientDao {

    /**
     * Save patient entity.
     *
     * @param patient    patient to save.
     * @param connection Db connection.
     */
    void save(Patient patient, Connection connection);

    /**
     * Find patient by id.
     *
     * @param id patient id.
     * @return patient entity.
     */
    Patient findById(int id);

    /**
     * Delete patient by id.
     *
     * @param id         patient id.
     * @param connection Db connection.
     */
    void deleteById(int id, Connection connection);

    /**
     * Find all patients with diagnosis.
     *
     * @return list of patient entities.
     */
    List<Patient> findAllWithDiagnosis();

    /**
     * Find all patients without card by doctor id.
     *
     * @param id doctor id.
     * @return list of patient entities.
     */
    List<Patient> findAllWithoutCardByDoctorId(int id);

    /**
     * Find all patients with diagnosis by doctor id.
     *
     * @param id doctor id.
     * @return list of patient entities.
     */
    List<Patient> findAllWithDiagnosisByDoctorId(int id);

    /**
     * Find all patients.
     *
     * @return list of patient entities.
     */
    List<Patient> findAllPatients();
}
