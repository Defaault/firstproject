package ua.nure.ipatov.summarytask.dao;

import ua.nure.ipatov.summarytask.entity.Doctor;

import java.sql.Connection;
import java.util.List;

/**
 * Data access object for doctor entity
 */
public interface DoctorDao {

    /**
     * Save doctor entity.
     *
     * @param doctor     doctor to save.
     * @param connection Db connection.
     */
    void save(Doctor doctor, Connection connection);

    /**
     * Find all doctors.
     *
     * @return list of doctor entities.
     */
    List<Doctor> findAllDoctors();

    /**
     * Increment count of patients.
     *
     * @param id         doctor id.
     * @param connection Db connection.
     */
    void incrementCountPatients(int id, Connection connection);

    /**
     * Decrement count of patients.
     *
     * @param id         doctor id.
     * @param connection Db connection.
     */
    void decrementCountPatients(int id, Connection connection);

    /**
     * Find doctor by user id.
     *
     * @param id user id.
     * @return doctor entity.
     */
    Doctor findByUserId(int id);

    /**
     * Find doctor id with minimum count of patients.
     *
     * @return doctor id.
     */
    int findDoctorIdWithMinPatient();

    /**
     * Find doctor id with minimum count of patients by specialty.
     *
     * @param selectRandom doctor specialty.
     * @return doctor id.
     */
    int findDoctorIdBySpecialtyWithMinPatient(String selectRandom);
}
