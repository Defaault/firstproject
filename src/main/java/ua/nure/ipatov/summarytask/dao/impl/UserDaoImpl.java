package ua.nure.ipatov.summarytask.dao.impl;

import org.apache.log4j.Logger;
import ua.nure.ipatov.summarytask.constant.RoleEnum;
import ua.nure.ipatov.summarytask.dao.UserDao;
import ua.nure.ipatov.summarytask.entity.User;
import ua.nure.ipatov.summarytask.util.DBManagerUtil;

import java.sql.*;

import static ua.nure.ipatov.summarytask.constant.Query.*;

public class UserDaoImpl implements UserDao {

    private static final Logger log = Logger.getLogger(UserDaoImpl.class);

    @Override
    public int save(User user, Connection connection) {

        try (PreparedStatement preparedStatement = connection.prepareStatement(SAVE_USER, Statement.RETURN_GENERATED_KEYS)) {

            preparedStatement.setString(1, user.getLogin());
            preparedStatement.setString(2, user.getPassword());
            preparedStatement.setInt(3, user.getRoleEnum().getId());
            preparedStatement.executeUpdate();
            try (ResultSet resultSet = preparedStatement.getGeneratedKeys()) {

                if (resultSet.next()) {
                    return resultSet.getInt(1);
                }
            }

        } catch (SQLException e) {
            log.error(e.getMessage());

            try {
                connection.rollback();
            } catch (SQLException ex) {
                log.error(ex.getMessage());
            }
        }
        return 0;
    }

    @Override
    public User findByLogin(String login) {

        try (Connection connection = DBManagerUtil.getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(TAKE_USER)) {

            preparedStatement.setString(1, login);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {

                if (resultSet.next()) {
                    User user = new User();
                    user.setId(resultSet.getInt(1));
                    user.setLogin(resultSet.getString(2));
                    user.setPassword(resultSet.getString(3));
                    user.setLocale(resultSet.getString(4));
                    user.setRoleEnum(RoleEnum.valueOf(resultSet.getString(6)));
                    return user;
                }
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return null;
    }

    @Override
    public void updateLocale(int id, String locale) {

        try (Connection connection = DBManagerUtil.getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_LOCALE)) {

            preparedStatement.setString(1, locale);
            preparedStatement.setInt(2, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            log.error(e.getMessage());
        }

    }
}
