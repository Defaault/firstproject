package ua.nure.ipatov.summarytask.service.impl;

import org.apache.log4j.Logger;
import ua.nure.ipatov.summarytask.dao.DoctorDao;
import ua.nure.ipatov.summarytask.dao.PatientDao;
import ua.nure.ipatov.summarytask.entity.Patient;
import ua.nure.ipatov.summarytask.service.PatientService;
import ua.nure.ipatov.summarytask.util.DBManagerUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import static java.util.Objects.nonNull;
import static ua.nure.ipatov.summarytask.constant.Constant.ALPHABET;
import static ua.nure.ipatov.summarytask.constant.Constant.BIRTHDAY;

public class PatientServiceImpl implements PatientService {

    private PatientDao patientDao;
    private DoctorDao doctorDao;
    private static final Logger log = Logger.getLogger(PatientServiceImpl.class);

    public PatientServiceImpl(PatientDao patientDao, DoctorDao doctorDao) {
        this.patientDao = patientDao;
        this.doctorDao = doctorDao;
    }

    @Override
    public void create(Patient patient) {
        try (Connection connection = DBManagerUtil.getInstance().getConnection()) {
            connection.setAutoCommit(false);
            patientDao.save(patient, connection);
            doctorDao.incrementCountPatients(patient.getDoctorId(), connection);
            connection.commit();
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public List<Patient> getAllPatients() {
        return patientDao.findAllPatients();
    }

    @Override
    public List<Patient> getPatientsWithDiagnosis() {
        return patientDao.findAllWithDiagnosis();
    }

    @Override
    public List<Patient> getPatientsWithoutCardByDoctorId(int id) {
        return patientDao.findAllWithoutCardByDoctorId(id);
    }

    @Override
    public List<Patient> getPatientsWithDiagnosisByDoctorId(int id) {
        return patientDao.findAllWithDiagnosisByDoctorId(id);
    }

    @Override
    public List<Patient> sort(List<Patient> list, String sort) {
        if (nonNull(list)) {
            if (ALPHABET.equals(sort)) {
                return alphabetSort(list);
            } else if (BIRTHDAY.equals(sort)) {
                return birthdaySort(list);
            }
        }
        return list;
    }

    @Override
    public Patient getPatientById(int id) {
        return patientDao.findById(id);
    }

    @Override
    public int createRandomly(Patient patient, String selectRandom) {

        int doctorId;

        if ("all".equals(selectRandom)) {
            doctorId = doctorDao.findDoctorIdWithMinPatient();
        } else {
            doctorId = doctorDao.findDoctorIdBySpecialtyWithMinPatient(selectRandom);
        }

        if (doctorId != 0) {
            patient.setDoctorId(doctorId);
            try (Connection connection = DBManagerUtil.getInstance().getConnection()) {
                connection.setAutoCommit(false);
                patientDao.save(patient, connection);
                doctorDao.incrementCountPatients(doctorId, connection);
                connection.commit();
            } catch (SQLException e) {
                log.error(e.getMessage());
            }
            return 0;
        } else {
            return 1;
        }
    }

    private List<Patient> alphabetSort(List<Patient> list) {
        list.sort((patient, t1) -> Integer.compare(patient.getName().compareTo(t1.getName()), 0));
        return list;
    }

    private List<Patient> birthdaySort(List<Patient> list) {
        list.sort((patient, t1) -> Integer.compare(patient.getBirthday().compareTo(t1.getBirthday()), 0));
        return list;
    }
}
