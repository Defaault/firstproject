package ua.nure.ipatov.summarytask.constant;

public enum ActionType {
    GET, POST
}
