package ua.nure.ipatov.summarytask.service;

import ua.nure.ipatov.summarytask.entity.Patient;

import java.util.List;

/**
 * Service for Patient entity
 */
public interface PatientService {

    /**
     * Create patient.
     *
     * @param patient patient to create.
     */
    void create(Patient patient);

    /**
     * Get all patients.
     *
     * @return list of patient entities.
     */
    List<Patient> getAllPatients();

    /**
     * Get all patients with diagnosis.
     *
     * @return list of patient entities
     */
    List<Patient> getPatientsWithDiagnosis();

    /**
     * Get all patients without card by doctor id.
     *
     * @param id doctor id.
     * @return list of patient entities.
     */
    List<Patient> getPatientsWithoutCardByDoctorId(int id);

    /**
     * Get all patients with diagnosis by doctor id.
     *
     * @param id doctor id.
     * @return list of patient entities.
     */
    List<Patient> getPatientsWithDiagnosisByDoctorId(int id);

    /**
     * Get patient by id.
     *
     * @param id patient id.
     * @return patient entity.
     */
    Patient getPatientById(int id);

    /**
     * Sort list of patients.
     *
     * @param list list of patient entities.
     * @param sort type of sort.
     * @return sorted list of patient entities.
     */
    List<Patient> sort(List<Patient> list, String sort);

    /**
     * Add patient to random doctor
     *
     * @param patient      patient entity.
     * @param selectRandom specialty doctor.
     * @return status of executing.
     */
    int createRandomly(Patient patient, String selectRandom);
}
