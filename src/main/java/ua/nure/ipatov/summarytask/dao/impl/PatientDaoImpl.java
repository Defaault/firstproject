package ua.nure.ipatov.summarytask.dao.impl;

import org.apache.log4j.Logger;
import ua.nure.ipatov.summarytask.dao.PatientDao;
import ua.nure.ipatov.summarytask.entity.Patient;
import ua.nure.ipatov.summarytask.util.DBManagerUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static ua.nure.ipatov.summarytask.constant.Query.*;

public class PatientDaoImpl implements PatientDao {

    private static final Logger log = Logger.getLogger(PatientDaoImpl.class);

    @Override
    public void save(Patient patient, Connection connection) {

        try (PreparedStatement preparedStatement = connection.prepareStatement(PATIENTS_SAVE)) {

            preparedStatement.setString(1, patient.getName());
            preparedStatement.setString(2, patient.getSurname());
            preparedStatement.setDate(3, patient.getBirthday());
            preparedStatement.setInt(4, patient.getDoctorId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            log.error(e.getMessage());

            try {
                connection.rollback();
            } catch (SQLException ex) {
                log.error(ex.getMessage());
            }
        }
    }

    @Override
    public Patient findById(int id) {

        try (Connection connection = DBManagerUtil.getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(FIND_PATIENT)) {

            preparedStatement.setInt(1, id);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {

                if (resultSet.next()) {
                    Patient patient = new Patient();
                    patient.setId(resultSet.getInt(1));
                    patient.setName(resultSet.getString(2));
                    patient.setSurname(resultSet.getString(3));
                    patient.setBirthday(resultSet.getDate(4));
                    patient.setDoctorId(resultSet.getInt(5));
                    return patient;
                }
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return null;
    }

    @Override
    public void deleteById(int id, Connection connection) {

        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE_BY_ID)) {

            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            log.error(e.getMessage());

            try {
                connection.rollback();
            } catch (SQLException ex) {
                log.error(ex.getMessage());
            }
        }
    }

    @Override
    public List<Patient> findAllWithDiagnosis() {
        List<Patient> list = new ArrayList<>();

        try (Connection connection = DBManagerUtil.getInstance().getConnection();
             Statement statement = connection.createStatement()) {

            try (ResultSet resultSet = statement.executeQuery(FIND_ALL_WITH_DIAGNOSIS)) {

                while (resultSet.next()) {
                    Patient patient = new Patient();
                    patient.setId(resultSet.getInt(1));
                    patient.setName(resultSet.getString(2));
                    patient.setSurname(resultSet.getString(3));
                    patient.setBirthday(resultSet.getDate(4));
                    patient.setDoctorId(resultSet.getInt(5));
                    list.add(patient);
                }
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return list;
    }

    @Override
    public List<Patient> findAllWithoutCardByDoctorId(int id) {

        List<Patient> list = new ArrayList<>();

        try (Connection connection = DBManagerUtil.getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(FIND_ALL_WITHOUT_CARD_BY_DOCTOR_ID)) {

            preparedStatement.setInt(1, id);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {

                while (resultSet.next()) {
                    Patient patient = new Patient();
                    patient.setId(resultSet.getInt(1));
                    patient.setName(resultSet.getString(2));
                    patient.setSurname(resultSet.getString(3));
                    patient.setBirthday(resultSet.getDate(4));
                    patient.setDoctorId(resultSet.getInt(5));
                    list.add(patient);
                }
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return list;
    }

    @Override
    public List<Patient> findAllWithDiagnosisByDoctorId(int id) {
        List<Patient> list = new ArrayList<>();

        try (Connection connection = DBManagerUtil.getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(FIND_ALL_WITH_DIAGNOSIS_BY_DOCTOR_ID)) {

            preparedStatement.setInt(1, id);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {

                while (resultSet.next()) {
                    Patient patient = new Patient();
                    patient.setId(resultSet.getInt(1));
                    patient.setName(resultSet.getString(2));
                    patient.setSurname(resultSet.getString(3));
                    patient.setBirthday(resultSet.getDate(4));
                    patient.setDoctorId(resultSet.getInt(5));
                    list.add(patient);
                }
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return list;
    }

    @Override
    public List<Patient> findAllPatients() {

        List<Patient> list = new ArrayList<>();

        try (Connection connection = DBManagerUtil.getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(FIND_ALL_PATIENTS)) {

            try (ResultSet resultSet = preparedStatement.executeQuery()) {

                while (resultSet.next()) {
                    Patient patient = new Patient();
                    patient.setId(resultSet.getInt(1));
                    patient.setName(resultSet.getString(2));
                    patient.setSurname(resultSet.getString(3));
                    patient.setBirthday(resultSet.getDate(4));
                    patient.setDoctorId(resultSet.getInt(5));
                    list.add(patient);
                }
                return list;
            }

        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return null;
    }
}
